package edu.upc.essi.gps.ecommerce;

import java.text.ParseException;
import java.util.List;

public class StoreManager extends User {
    //També conegut com a cap de botiga
    public StoreManager(String username, String password, long id) {
        super(username, password, id);
    }

    public void newWorkerUser(String name, String password, PosController pc){
        if(name.isEmpty())  throw new IllegalStateException("Has d'introduir un nom d'usuari");
        if(password.isEmpty())  throw new IllegalStateException("Has d'introduir una contrasenya");
        try {
            pc.getUserController().newWorker(name,password);
        } catch (ParseException e) {
            //  e.printStackTrace();
        }
    }
    @Override
    public void addNewProduct(int barCode, ProductsService productsService) {
        productsService.newProduct(barCode);
    }

    @Override
    public void addNewProduct(String name, double price, int iva, int barCode, ProductsService productsService) {
        productsService.newProduct(name,price,iva,barCode);
    }
    @Override
    public void newProductWithBrand(String name, String brand, double price, int iva, int barCode, ProductsService productsService, DiscountRepository discountRepository) {
        productsService.newProductWithBrand(name, price, iva, barCode, brand);
        Product p = productsService.findByBarCode(barCode);
        List<Discount> list = discountRepository.list();
        for(Discount d : list){
            String aux = d.getBrandDiscount();
            if(aux.equals(brand)){
                d.addProductToDiscount(p);
            }
        }
    }

    @Override
    public void setPriceToProduct(double price, int barCode, ProductsService productsService) {
        productsService.findByBarCode(barCode).setPrice(price);
    }
    @Override
    public void setVatPct(int iva, int barCode, ProductsService productsService) {
        productsService.findByBarCode(barCode).setVatPct(iva);
    }
    @Override
    public void setProductName(String name, int barCode, ProductsService productsService) {
        productsService.findByBarCode(barCode).setName(name);
    }
}
