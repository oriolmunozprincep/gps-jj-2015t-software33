package edu.upc.essi.gps.ecommerce;

import javafx.util.Pair;

import java.text.ParseException;
import java.util.*;

import static edu.upc.essi.gps.utils.Validations.*;

public class PosController {

    private final ProductsService productsService;
    private final LoyalCostumerService loyalCostumerService;
    private String shop;
    private final String address;
    private int posNumber;
    private Sale currentSale;
    private WorkingDayController workingDayController;
    private WorkingDay currentWorkingDay;
    private int retry;
    private DiscountRepository discountRepository;
    private UserController userController;
    private User  currentSaleAssistant;
    private LoyalCostumer currentLoyalCostumer;
    private boolean hadPoints = false;

    public PosController(String shop, String address, int posNumber) {
        this.shop = shop;
        this.address = address;
        this.posNumber = posNumber;
        this.productsService = new ProductsService(new ProductsRepository());
        this.currentWorkingDay= null;
        this.workingDayController = new WorkingDayController();
        this.discountRepository = new DiscountRepository();
        this.userController = new UserController();
        this.loyalCostumerService = new LoyalCostumerService(new LoyalCostumerRepository());
    }

    public void login(String saleAssistantName, String password) {
        checkNotNull(saleAssistantName, "saleAssistantName");
        if (this.currentSaleAssistant != null) throw new IllegalStateException("Aquest tpv està en ús per " + this.currentSaleAssistant.getUsername());
        if(saleAssistantName.isEmpty())throw new IllegalStateException("Has d'introduir un nom d'usuari");
        User logUser = userController.findByName(saleAssistantName);
        if(logUser != null) {
            if(logUser.getPassword().equals(password))this.currentSaleAssistant = logUser;
            else throw new IllegalStateException("La contrasenya està malament");
        }
        else throw new IllegalStateException("No hi ha cap empleat amb aquest nom");
    }

    public void logout() {
        if(this.currentSaleAssistant==null) throw new IllegalStateException("No hi ha cap sessió iniciada");
        if(currentWorkingDay!=null) throw new IllegalStateException("No es pot tancar sessió mentre hi hagi un torn sense tancar");
        this.currentSaleAssistant = null;
    }

    public void setShopName(String shopName) { this.shop = shopName; }

    public void setPosNumber(int posNumber) {
        this.posNumber = posNumber;
    }

    public String getShopName() {
        return this.shop;
    }

    public void startSale(Date date) {
        if(currentSaleAssistant==null) throw new IllegalStateException("No es pot iniciar una venda si no hi ha una sessió iniciada");
        currentSaleAssistant.startSale(date, this);
    }

    public void discardSale(){
        if(currentSaleAssistant==null) throw new IllegalStateException("No es pot descartar una venda si no hi ha una sessió iniciada");
        currentSaleAssistant.discardSale(this);
    }

    public void closeSale() {
        if(currentWorkingDay==null) throw new IllegalStateException("No es pot tancar una venda si no hi ha cap jornada iniciada");
        if(currentSale==null) throw new IllegalStateException("No es pot descartar una venda si no n'hi ha cap iniciada");
        currentWorkingDay.addSale(currentSale);
        currentSale=null;
    }

    public String getCurrentSaleAssistantName() {
        if(currentSaleAssistant==null) throw new IllegalStateException("No hi ha cap sessió iniciada");
        return currentSaleAssistant.getUsername();
    }

    public Sale getCurrentSale() {
        return currentSale;
    }



    public void removeProductByBarCode(int barCode) {
        if(currentSaleAssistant==null) throw new IllegalStateException("No es pot eliminar un producte si no hi ha una sessió iniciada");
        currentSaleAssistant.removeProductByBarCode(barCode, this);
    }

    public void addProductByBarCode(int barCode) {
        if(currentSaleAssistant==null) throw new IllegalStateException("No es pot iniciar una venda si no hi ha una sessió iniciada");
        currentSaleAssistant.addProductByBarCode(barCode, this);  }

    public void addProductByLine(int line) {
        if(currentSaleAssistant==null) throw new IllegalStateException("No es pot iniciar una venda si no hi ha una sessió iniciada");
        currentSaleAssistant.addProductByLine(line, this);
    }

    public String getCustomerScreenMessages() {
        String welcomeMessage = "Li donem la benvinguda a Joguets i Joguines!";
        if (currentSale == null) return welcomeMessage;
        if (currentSale.isEmpty()) {
            return welcomeMessage + "\nL'atén " + currentSale.getSaleAssistantName();
        }
        StringBuilder sb = new StringBuilder();
        for (SaleLine sl : currentSale.getLines()) {
            sb.append(sl.getProductName()).append(" - ")
                    .append(Math.floor(sl.getUnitPriceVat()*100+.5)/100).append("€/u x ").append(sl.getAmount()).append("u = ")
                    .append(Math.floor(sl.getTotalPriceVat()*100+.5)/100).append("€\n");
        }
        sb.append("---\n").append("Total: ").append(Math.floor(currentSale.getTotal() * 100 + .5)/ 100).append("€");
        return sb.toString();
    }

    public double payment(String paymentMethod,Double givenMoney) {
        if (currentSaleAssistant == null)  throw new IllegalStateException("No es pot fer un pagament si no hi ha una sessió iniciada");
        if (givenMoney < 1 & !paymentMethod.equals("Targeta")) throw new IllegalStateException("El client ha de aportar un minim de 1€");
        double total = currentSale.getTotal();
        double change = currentSaleAssistant.payment(paymentMethod, givenMoney, this);
        if (currentLoyalCostumer != null) {
            if (paymentMethod.equals("Targeta")){
                if (!hadPoints) currentLoyalCostumer.setPoints(total);
            }
            else {
                currentLoyalCostumer.setPoints(givenMoney-change);
            }
        }
        return change;
    }

    public void setSaleAmountProduct(int barCode, int newAmount) {
        if (currentSaleAssistant == null)  throw new IllegalStateException("No es pot modificar una venda si no hi ha una sessió iniciada");
        currentSaleAssistant.setSaleAmountProduct(barCode, newAmount, this);
    }

    public void startOfWorkingDay(Date workingDayDate, double initialMoney) {
        if(currentWorkingDay != null){
            throw new IllegalStateException("Ja hi ha un torn iniciat");
        }
        if (currentSaleAssistant == null)  throw new IllegalStateException("No es pot iniciar una jornada si no hi ha una sessió iniciada");
        currentSaleAssistant.startOfWorkingDay(workingDayDate, initialMoney, this);
    }

    public WorkingDay getWorkingDay(Date workingDayDate){
        WorkingDay returnedWorkingDay = workingDayController.getWorkingDay(workingDayDate);
        if (returnedWorkingDay==null)throw new IllegalStateException("No existeix una torn amb aquesta data y hora");
        return returnedWorkingDay;
    }

    public WorkingDay getCurrentWorkingDay(){
        if (this.currentWorkingDay==null)throw new IllegalStateException("No s'ha iniciat un torn");
        return this.currentWorkingDay;
    }

    public String printTicket(){
        if (currentSaleAssistant == null)  throw new IllegalStateException("No es pot imprimir un tiquet si no hi ha una sessió iniciada");
        return currentSaleAssistant.printTicket(this);
    }

    public String getExistingProductsMessage() {
        if (currentSaleAssistant == null)  throw new IllegalStateException("No es pot mostrar el missatge de productes si no hi ha una sessió iniciada");
        return currentSaleAssistant.getExistingProductsMessage(this);
    }

    public void doBalance(double actualMoney) {
        if (currentSale != null)  throw new IllegalStateException("No es pot tancar un torn si hi ha una venda iniciada");
        currentSaleAssistant.doBalance(actualMoney, this);
    }

    public void closeWorkingDay(double actualMoney, String msg) {
        this.currentWorkingDay.closeDay(actualMoney, msg);
        this.currentWorkingDay = null;
    }

    public ArrayList<Discount> individualDiscountsForSaleLine(SaleLine product){
        ArrayList<Discount> disc = new ArrayList<Discount>();
        List<NxMDiscount> nxMDiscounts = discountRepository.getAllNxMDiscount();
        Iterator<NxMDiscount> itNxM = nxMDiscounts.iterator();
        while (itNxM.hasNext()){
            NxMDiscount nxmDis = itNxM.next();
            if(nxmDis.isProductInDiscount(product.getProductId())) disc.add(nxmDis);
        }
        List<PercentDiscount> percentDiscounts = discountRepository.getAllPercentDiscount();
        Iterator<PercentDiscount> itPD = percentDiscounts.iterator();
        while (itPD.hasNext()){
            PercentDiscount perDis = itPD.next();
            if(perDis.isProductInDiscount(product.getProductId())) disc.add(perDis);
        }
        return disc;
    }

    public void productosDescuentoRec(ArrayList<Integer> saleProdId,Integer index ,NxMDiscount dis, ArrayList<Integer> solucion, ArrayList<ArrayList<Integer>> ret){
        if((index==saleProdId.size()) || (solucion.size()==dis.getN())){ }


        else {
            Integer prodID = saleProdId.get(index);
            productosDescuentoRec(saleProdId, index+1, dis, solucion, ret);
            if(dis.isProductInDiscount(prodID)){
                ArrayList<Integer> solTemp= new ArrayList<>();
                solTemp.add(prodID);
                boolean first=true;
                int firstFound=index;
                for(int i=index+1;i<saleProdId.size();++i){
                    if(solTemp.size()==dis.getN()){
                        ret.add(solTemp);
                        solTemp=new ArrayList<>();
                        solTemp.add(prodID);
                        i=firstFound+1;
                        first=true;
                    }
                    if(dis.isProductInDiscount(saleProdId.get(i))){
                        solTemp.add(saleProdId.get(i));
                        if(first){
                            firstFound=i;
                            first=false;
                        }
                    }
                }
                if(solTemp.size()==dis.getN())ret.add(solTemp);

            }

        }


    }

    public double bestDiscountsForSale (){
        double totalPrice = 0;
        List<SaleLine> products = currentSale.getLines();
        Iterator<SaleLine> itProducts = products.iterator();
        List<Discount> bestDiscounts = new ArrayList<Discount>();
        int i = 0;
        //busquem preu amb millors descomptes nxm i descompte sobre una marca
        while (itProducts.hasNext()){
            SaleLine prod = itProducts.next();
            double priceSaleLine = prod.getTotalPriceVat();//agafem el preu de la linea de venda sense decompte
            //agafem tots els descomptes posibles per a aquestaLinea de venda
            List<Discount> discountForSaleLine = individualDiscountsForSaleLine(prod);
            Iterator<Discount> itDiscuntsSaleLine = discountForSaleLine.iterator();
            bestDiscounts.add(i,null); // afegim null pq d' entrada no hi ha cap descompte per aqest producte

            while (itDiscuntsSaleLine.hasNext()){
                Discount posibleDiscount = itDiscuntsSaleLine.next();
                double priceSaleLineWithDiscount = posibleDiscount.priceWithDiscount(prod);
                if(priceSaleLine>=priceSaleLineWithDiscount){
                    priceSaleLine=priceSaleLineWithDiscount;
                    bestDiscounts.set(i, posibleDiscount);
                }
            }
            totalPrice+=priceSaleLine;
            i++;
        }

        //mirem si millora amb un descompte global
        int j = 0;
        List<FixDiscount> fixDiscounts = discountRepository.getAllFixDiscount();
        Iterator<FixDiscount> itFd = fixDiscounts.iterator();
        boolean fix = false;
        while (itFd.hasNext()){
            FixDiscount fdis = itFd.next();
            Double newpriece = currentSale.getTotalWithoutDiscounts()- fdis.getDiscountQuantity();
            if(newpriece>0 && newpriece<totalPrice){
                j = 1;
                bestDiscounts = new ArrayList<Discount>();
                bestDiscounts.add(fdis);
                totalPrice=newpriece;
                fix=true;
            }
        }

        //Afegim tots els regals
        List<SaleLine> productesGift = currentSale.getLines();
        Iterator<SaleLine> itProdGifts = productesGift.iterator();

        while(itProdGifts.hasNext()) {
            SaleLine prodGift = itProdGifts.next();
            List<GiftDiscount> presentDiscounts = discountRepository.getAllGiftDiscount();
            Iterator<GiftDiscount> itPG = presentDiscounts.iterator();

            boolean gifted = false;
            while (itPG.hasNext()) {
               GiftDiscount gdis = itPG.next();
               if(gdis.isGiftOfSale(productesGift, prodGift)){
                   gifted=true;
                   bestDiscounts.set(j,gdis);
               }
            }
            if(gifted) totalPrice-=prodGift.getUnitPriceVat();
            ++j;
        }

        currentSale.setAppliedDiscounts(bestDiscounts);

        //mirem 3x2 de productes

        if(!fix) {
            ArrayList<Integer> idProductes = new ArrayList<>();
            List<SaleLine> prodNxM = currentSale.getLines();
            Iterator<SaleLine> itProdNxM = prodNxM.iterator();
            int ind = 0;
            while (itProdNxM.hasNext()) {
                SaleLine prod = itProdNxM.next();
                if (bestDiscounts.get(ind) == null || bestDiscounts.get(ind).getDiscountType().equals("NxM")) {
                    bestDiscounts.set(ind, null);
                    int it = 0;
                    while (it < prod.getAmount()) {
                        idProductes.add(prod.getProductBarCode());
                        it++;
                    }
                }
                ind++;
            }

            if (discountRepository.getAllNxMDiscount().size() > 0) {
                NxMDiscount discountSpecial = discountRepository.getAllNxMDiscount().get(0);
                ArrayList<Integer> solucion = new ArrayList<>();
                ArrayList<ArrayList<Integer>> aux = new ArrayList<>();
                productosDescuentoRec(idProductes, 0, discountSpecial, solucion, aux);


                Iterator<ArrayList<Integer>> itsol = aux.iterator();
                ArrayList<Integer> aux2 = itsol.next();
                Iterator<Integer> itaux = aux2.iterator();
                ArrayList<Product> pr = new ArrayList<>();

                ArrayList<Pair<ArrayList<Product>, NxMDiscount>> nxmDiscountsMultiples = new ArrayList<>();
                if (aux2.size() > 0) {
                    while (itaux.hasNext()) {
                        pr.add(productsService.findByBarCode(itaux.next()));
                    }
                    Pair<ArrayList<Product>, NxMDiscount> pa = new Pair<>(pr, discountSpecial);
                    nxmDiscountsMultiples.add(pa);
                    currentSale.setSpecial(nxmDiscountsMultiples);
                }
            }
        }
        currentSale.setAppliedDiscounts(bestDiscounts);
        return  totalPrice;

    }

    //pasar a user
    public long newPercentDiscount(ArrayList<Product> products, Date initDate, Date finalDate, double discountRate) {
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un descompte si no hi ha una sessio iniciada");
        return currentSaleAssistant.newPercentDiscount(products, initDate, finalDate, discountRate, this);
    }

    public long newPercentDiscount(Date initDate, Date finalDate, String brand, ProductsService productsService, double discountRate) {
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un descompte si no hi ha una sessio iniciada");
        return currentSaleAssistant.newPercentDiscount(initDate, finalDate, brand, productsService, discountRate, this);
    }

    public long newFixDiscount(ArrayList<Product> products, Date initDate, Date finalDate, int discountQuantity) {
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un descompte si no hi ha una sessio iniciada");
        return currentSaleAssistant.newFixDiscount(products, initDate, finalDate, discountQuantity, this);
    }

    public long newFixDiscount( Date initDate, Date finalDate, String brand, ProductsService productsService, int discountQuantity) {
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un descompte si no hi ha una sessio iniciada");
        return  currentSaleAssistant.newFixDiscount(initDate, finalDate, brand, productsService, discountQuantity, this);
    }

    public long newNxMDiscount( ArrayList<Product> products, Date initDate, Date finalDate, int n, int m) {
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un descompte si no hi ha una sessio iniciada");
        return  currentSaleAssistant.newNxMDiscount(products, initDate, finalDate, n, m, this);
    }

    public long newNxMDiscount(Date initDate, Date finalDate, String brand, ProductsService productsService, int n, int m) {
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un descompte si no hi ha una sessio iniciada");
        return currentSaleAssistant.newNxMDiscount(initDate, finalDate, brand, productsService, n, m, this);
    }

    public long newGiftDiscount( ArrayList<Product> products, Date initDate, Date finalDate, ArrayList<Product> gifts) {
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un descompte si no hi ha una sessio iniciada");
        return currentSaleAssistant.newGiftDiscount(products, initDate, finalDate, gifts, this);
    }

    public long newGiftDiscount( Date initDate, Date finalDate, String brand, ProductsService productsService, ArrayList<Product> gifts) {
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un descompte si no hi ha una sessio iniciada");
       return currentSaleAssistant.newGiftDiscount(initDate, finalDate, brand, productsService, gifts, this);
    }

    public long newVoucher(Date initDate, Date finalDate, int discountQuantity, int minimumPay, int barCode){
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un val de descompte si no hi ha una sessio iniciada");
        return currentSaleAssistant.newVoucher(initDate, finalDate, discountQuantity, minimumPay, barCode, this);
    }

    public void newLoyalCostumer(String phoneNumber, String name){
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un client fidelitzat si no hi ha una sessio iniciada");
        currentSaleAssistant.newLoyalCostumer(phoneNumber, name, this);
    }

    public void newLoyalCostumer(String phoneNumber, String name, int points){
        if (currentSaleAssistant== null)  throw new IllegalStateException("No es pot donar d'alta un client fidelitzat si no hi ha una sessio iniciada");
        currentSaleAssistant.newLoyalCostumer(phoneNumber, name, points, this);
    }

    public Discount getDiscount(long id){
        return this.discountRepository.findById(id);
    }

    public ProductsService getProductsService () {
        return this.productsService;
    }

    public DiscountRepository getDiscountRepository(){
        return this.discountRepository;
    }

    public LoyalCostumerService getLoyalCostumerService(){
        return this.loyalCostumerService;
    }

    public void firstAdministratorUser(String username, String password) {  //es una mica brut, però nose com fer el registre del primer admin sino
        //if(!this.userController.findByName(this.currentSaleAssistant.getUsername()).isAdmin())throw new IllegalStateException("Només els administradors poden afegir nous usuaris");
        if(username.isEmpty())  throw new IllegalStateException("Has d'introduir un nom d'usuari");
        if(password.isEmpty())  throw new IllegalStateException("Has d'introduir una contrasenya");
        try {
            this.userController.newAdministrator(username,password);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void newAdministratorUser(String name, String password) {
        if(this.currentSaleAssistant==null) throw new IllegalStateException("No es pot donar d'alta un nou administrador si no hi ha cap sessió iniciada");
        currentSaleAssistant.newAdministratorUser(name,password, this);
    }

    public void newWorkerUser(String name, String password){
        if(this.currentSaleAssistant==null) throw new IllegalStateException("No es pot donar d'alta un nou treballador si no hi ha cap sessió iniciada");
            currentSaleAssistant.newWorkerUser(name, password, this);
    }

    public void newStoreManegerUser(String name, String password){
        if(this.currentSaleAssistant==null) throw new IllegalStateException("No es pot donar d'alta un nou cap de botiga si no hi ha cap sessió iniciada");
        currentSaleAssistant.newStoreManegerUser(name, password, this);
    }

    public UserController getUserController() {
        return userController;
    }

    public void setCurrentSale(Sale currentSale) {
        this.currentSale = currentSale;
    }

    public String getAddress() {
        return address;
    }

    public int getPosNumber() {
        return posNumber;
    }

    public User getWorkerUser(String name){
        return userController.findByName(name);
    }

    public WorkingDayController getWorkingDayController() {
        return workingDayController;
    }

    public void setRetry(int retry) {
        this.retry = retry;
    }

    public void setCurrentWorkingDay(WorkingDay currentWorkingDay) {
        this.currentWorkingDay = currentWorkingDay;
    }

    public int getRetry() {
        return retry;
    }

    public void addNewProduct(String name, double price, int iva, int barCode) {
        if(currentSaleAssistant!=null) currentSaleAssistant.addNewProduct(name,price,iva,barCode,productsService);
        else throw new IllegalArgumentException("No es poden afegir productes si no s'ha iniciat sessió");
    }

    public void newProductWithBrand(String name, String brand, double price, int iva, int barCode) {
        if(currentSaleAssistant!=null) currentSaleAssistant.newProductWithBrand(name,brand,price,iva,barCode,productsService,discountRepository);
        else throw new IllegalArgumentException("No es poden afegir productes si no s'ha iniciat sessió");
    }

    public void addNewProduct(int barCode){
        if(currentSaleAssistant!=null) currentSaleAssistant.addNewProduct(barCode,productsService);
        else throw new IllegalArgumentException("No es poden afegir productes si no s'ha iniciat sessió");
    }

    public void applyVoucher(int barCode) {
        Voucher v = getDiscountRepository().findVoucherByBarCode(barCode);//findVoucher(discountQuantity,minimumPay);
        Date now = new Date();
        if (v == null) throw new IllegalArgumentException("No es un val de descompte correcte");
        else if (!v.isInDate(now)) throw new IllegalArgumentException("Val caducat");
        else if (getCurrentSale().getTotal() < v.getMinimumPay()) throw  new IllegalArgumentException("No es aplicable amb aquest cost de compra");
        List<Voucher> lv = getCurrentSale().getAppliedVouchers();
        lv.add(v);
        getCurrentSale().setAppliedVouchers(lv);
    }

    public void setPriceToProduct(double price, int barCode) {
        if(currentSaleAssistant!=null) currentSaleAssistant.setPriceToProduct(price, barCode, productsService);
        else throw new IllegalArgumentException("No es poden modificar productes si no s'ha iniciat sessió");
    }

    public void setVatPct(int barCode, int iva) {
        if(currentSaleAssistant!=null) currentSaleAssistant.setVatPct(iva, barCode, productsService);
        else throw new IllegalArgumentException("No es poden modificar productes si no s'ha iniciat sessió");
    }

    public void setProductName(int barCode, String name) {
        if(currentSaleAssistant!=null) currentSaleAssistant.setProductName(name, barCode, productsService);
        else throw new IllegalArgumentException("No es poden modificar productes si no s'ha iniciat sessió");
    }

    public void setLineAmount(int line, int amount) {
        if(currentSaleAssistant!=null) currentSaleAssistant.setLineAmount(line, amount, this);
        else throw new IllegalArgumentException("No es poden modificar la quantitat de productes d'una venda si no s'ha iniciat sessió");
    }

    public void setGivenMoney(double money) {
        if(currentSaleAssistant!=null) currentSaleAssistant.setGivenMoney(money, this);
        else throw new IllegalArgumentException("No es poden modificar els diners rebuts d'una venda si no s'ha iniciat sessió");
    }

    public Product findProductByName(String productName) {
        return productsService.findByName(productName);
    }

    public ArrayList<Product> findProductByBrand(String brand) {
        return productsService.findByBrand(brand);
    }

    public String getAllSalesReport() {
        return getSalesReportOfDay(null);
    }

    public String getSalesReportOfDay(Date date) {
        if (currentSaleAssistant.isAllowedToSeeReports()) {
            final SalesReportPrinter printer = new SalesReportPrinter();
            if (date != null) return printer.print(workingDayController.findByDay(date));
            else return printer.print(workingDayController.list());
        }
        else {
            throw new IllegalArgumentException("Només els caps de botiga poden veure els informes de vendes");
        }
    }

    public void setLoyalCostumer(String phoneNumber){
        LoyalCostumer lc = getLoyalCostumerService().findByPhoneNumber(phoneNumber);
        if (lc == null) {
            throw new IllegalArgumentException("No existeix cap client fidelitzat amb aquest numero de telefon");
        }
        currentLoyalCostumer = lc;
        setLoyalCostumerPoints();
    }

    public void setLoyalCostumerPoints(){
        if (currentLoyalCostumer.getPoints() >= 1000){
            hadPoints = true;
            getCurrentSale().setMoneyDiscountedByLoyal((double)currentLoyalCostumer.getPoints()/1000);
            currentLoyalCostumer.setPoints(0);
        }
    }
}