package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

/**
 * Created by Alejandro Hidalgo on 02/12/2015.
 */
public class GiftDiscount extends Discount{

    private ArrayList<Product> gifts;

    public GiftDiscount(long id, ArrayList<Product> products, Date initDate, Date finalDate, ArrayList<Product> gifts) throws IllegalStateException {
        super(id, products, initDate, finalDate);
        this.gifts = gifts;
    }

    public GiftDiscount(long id, Date initDate, Date finalDate, String brand, ProductsService productsService, ArrayList<Product> gifts) throws IllegalStateException {
        super(id, initDate, finalDate, brand, productsService);
        this.gifts = gifts;
    }

    public boolean isGift(ArrayList<Product>productsToBuy, Product productGifted){ //solo elementos correctos en el products to buy
        for (Product p : productsToBuy){
            if (!this.isProductInDiscount(p.getId())) return false;
        }

        for(Product p : gifts){
            if(p.getId() == productGifted.getId()) return true;
        }
        return false;
    }

    public boolean isGiftOfSale(List<SaleLine> productsToBuy, SaleLine productGifted){//pasmos todos losproductos
        boolean valid = false;

        for (SaleLine p : productsToBuy){
            if (this.isProductInDiscount(p.getProductId())) valid=true;
        }
        if(!valid)return false;

        for(Product p : gifts){
            if(p.getId() == productGifted.getProductId()) return true;
        }
        return false;
    }

    public ArrayList<Product> getGifts() {
        return gifts;
    }

    public String getDiscountType(){
        return "Regal";
    }

    @Override
    public String getTicketDiscountLine() {
        StringJoiner joiner = new StringJoiner(",");
        for (Product gift : gifts) {
            joiner.add(gift.getName());
        }
        return "Regal de " + joiner.toString();
    }

}
