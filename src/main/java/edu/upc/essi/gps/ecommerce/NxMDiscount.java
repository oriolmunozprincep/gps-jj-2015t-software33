package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.Date;
import java.util.StringJoiner;

/**
 * Created by Alejandro Hidalgo on 02/12/2015.
 */
public class NxMDiscount extends Discount {

    private int n;
    private int m;
    private String brand = null;

    public NxMDiscount(long id, ArrayList<Product> products, Date initDate, Date finalDate, int n, int m) throws IllegalStateException {
        super(id, products, initDate, finalDate);
        if (n == m)
            throw new IllegalStateException("n i m no poden ser iguals");
        if (n < m)
            throw new IllegalStateException("n no pot ser mes petit que m");
        this.n = n;
        this.m = m;
    }

    public NxMDiscount(long id, Date initDate, Date finalDate, String brand, ProductsService productsService, int n, int m) throws IllegalStateException {
        super(id, initDate, finalDate, brand, productsService);
        this.brand = brand;
        this.n = n;
        this.m = m;
    }

    public int[] getNM(){
        int[] aux = {n,m};
        return aux;
    }

    public int getN(){
        return n;
    }

    public String getDiscountType(){
        return "NxM";
    }

    @Override
    public String getTicketDiscountLine() {
        if (brand != null) return n + "x" + m + " en marca " + this.brand;
        else if (getProductList().size() > 1) return n + "x" + m + " en " + allProductsNames();
        else return n + "x" + m + " en el producte " + getProductList().get(0).getName();
    }

    public double priceWithDiscount (SaleLine products){
        isProductInDiscount(products.getProductId());
        int numberGiftProducts = products.getAmount() / n;
        return (products.getUnitPriceVat()*(products.getAmount()- numberGiftProducts));
    }

    public double discountAmmount (ArrayList<Product> products ){
        double minPrice= products.get(0).getPriceVat();
        for(int i= 1; i < products.size(); ++i){
            if(products.get(i).getPriceVat() < minPrice) minPrice=products.get(i).getPriceVat();
        }
        return minPrice;
    }
}
