package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.Date;

public class Voucher extends Discount{

    private int discountQuantity;
    private int minimumPay;
    private final int barCode;

    public Voucher(long id, Date initDate, Date finalDate, int discountQuantity, int minimumPay, int barCode) {
        super(id, new ArrayList<>(), initDate, finalDate);
        if (discountQuantity <= 0) throw new IllegalStateException("El descompte a de ser major a 0");
        if (minimumPay <= 0) throw new IllegalStateException("El pagament minim a de ser major a 0");
        if(discountQuantity>=minimumPay) throw new IllegalStateException("El pagament minim a de ser més gran que el descompte");
        this.discountQuantity = discountQuantity;

        this.minimumPay = minimumPay;
        this.barCode = barCode;
    }

    @Override
    public int getBarCode(){return barCode;}

    public int getDiscountQuantity(){
        return this.discountQuantity;
    }

    public String getDiscountType(){
        return "Voucher";
    }

    public int getMinimumPay() { return minimumPay; }

    @Override
    public String getTicketDiscountLine() {
        return discountQuantity+"€ de descompte per una compra mínima de "+minimumPay+"€";
    }

}
