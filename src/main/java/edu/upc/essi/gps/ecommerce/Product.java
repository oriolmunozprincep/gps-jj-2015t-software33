package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.Entity;
import edu.upc.essi.gps.domain.HasName;

public class Product implements Entity, HasName {

    private long id;
    private String name;
    private double price;
    private int vatPct;
    private final int barCode;
    private String brand;

    private float popularity;

    public Product(long id, String name, double price, int vatPct, int barCode) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.vatPct = vatPct;
        this.barCode = barCode;
        this.brand = null;
    }

    public Product(long id, String name, double price, int vatPct, int barCode, String brand) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.vatPct = vatPct;
        this.barCode = barCode;
        this.brand = brand;
    }

    public Product(long id, int barCode){
        this.id = id;
        this.barCode = barCode;
    }

    public boolean hasBrand() {
        if(brand==null)return false;
        else return true;
    }

    public String getBrand() {
        return brand;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getPopularity() {
        return popularity;
    }

    public double getPrice() {
        return price;
    }

    public int getVatPct() {
        return vatPct;
    }

    public int getBarCode() {
        return barCode;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setVatPct(int vatPct) {
        this.vatPct = vatPct;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPriceVat(){
        return (price+ price*((double)vatPct/100.0));
    }
}
