package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.Date;

public class Worker extends User {
    //També conegut com a treballador
    public Worker(String username, String password, long id) {
        super(username, password, id);
    }

    public void newPercentDiscount(int id, ArrayList<Product> products, Date initDate, Date finalDate, double discountRate, PosController pc) throws Exception {
        throw new Exception("No pots donar d'alta nous descomptes");
    }

    public void newPercentDiscount(int id, Date initDate, Date finalDate, String brand, ProductsService productsService, double discountRate, PosController pc) throws Exception {
        throw new Exception("No pots donar d'alta nous descomptes");
    }

    public void newFixDiscount(int id, ArrayList<Product> products, Date initDate, Date finalDate, int discountQuantity, PosController pc) throws Exception {
        throw new Exception("No pots donar d'alta nous descomptes");
    }

    public void newFixDiscount(int id, Date initDate, Date finalDate, String brand, ProductsService productsService, int discountQuantity, PosController pc) throws Exception {
        throw new Exception("No pots donar d'alta nous descomptes");
    }

    public void newNxMDiscount(int id, ArrayList<Product> products, Date initDate, Date finalDate, int n, int m, PosController pc) throws Exception {
        throw new Exception("No pots donar d'alta nous descomptes");
    }

    public void newNxMDiscount(int id, Date initDate, Date finalDate, String brand, ProductsService productsService, int n, int m, PosController pc) throws Exception {
        throw new Exception("No pots donar d'alta nous descomptes");
    }

    public void newGiftDiscount(int id, ArrayList<Product> products, Date initDate, Date finalDate, ArrayList<Product> gifts, PosController pc) throws Exception {
        throw new Exception("No pots donar d'alta nous descomptes");
    }

    public void newGiftDiscount(int id, Date initDate, Date finalDate, String brand, ProductsService productsService, ArrayList<Product> gifts, PosController pc) throws Exception {
        throw new Exception("No pots donar d'alta nous descomptes");
    }

}
