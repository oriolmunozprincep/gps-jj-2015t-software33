package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.Entity;
import edu.upc.essi.gps.domain.HasName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

public abstract class Discount implements Entity{
    //Identificador
    private long id;
    //Productes als que referencia
    private ArrayList<Product> productList;
    //Data de duració del descompte
    private Date initDate;
    private Date finalDate;
    //Si es valid o no (no sera valid si j'ha s'ha usat, no te en compte la data)
    private boolean validity;
    private String brand;


    public Discount(long id, ArrayList<Product> products, Date initDate, Date finalDate){
        this.id = id;
        this.productList = products;
        this.initDate = initDate;
        this.finalDate = finalDate;
        this.validity = true;
        this.brand = "";
    }

    public Discount(long id, Date initDate, Date finalDate, String brand, ProductsService productsService){
        this.id = id;
        this.initDate = initDate;
        this.finalDate = finalDate;
        this.validity = true;
        this.brand = brand;
        this.productList = productsService.findByBrand(brand);
    }

    //public functions

    public long getId () {
        return this.id;
    }

    public Date getInitDate() {
        return initDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public boolean isValid() {
        return this.validity;
    }

    public void setValidity(boolean validity) {
        this.validity = validity;
    }

    public boolean isInDate (Date date){
        if (initDate==null && isValid()){
            return true;
        }
        else {
            return (initDate.before(date) && finalDate.after(date) && isValid());
        }
    }

    public List<Product> getProductList() {
        return productList;
    }

    //Si productList.isEmpty() significa que el descompte es per a tota la venta i retornara true
    public boolean isProductInDiscount(long idProduct) {
        if (productList.isEmpty()) return true;
        for (Product p: productList){
            if (p.getId() == idProduct) return true;
        }
        return false;
    }
    public boolean isProductInDiscount(Integer barCode) {
        if (productList.isEmpty()) return true;
        for (Product p: productList){
            if (p.getBarCode() == barCode) return true;
        }
        return false;
    }
    public String getBrandDiscount(){
        return this.brand;
    }

    public void addProductToDiscount(Product p){
        productList.add(p);
    }

    //abstract functions
    public  double priceWithDiscount (SaleLine products){
        return 0;
    }

    public abstract String getDiscountType();

    public abstract String getTicketDiscountLine();

    public String getReportLine() {
        return getTicketDiscountLine();
    }

    protected String allProductsNames() {
        StringJoiner joiner = new StringJoiner(","); //God bless java 8
        for (Product p : this.getProductList()) {
            joiner.add(p.getName());
        }
        return joiner.toString();
    }

    public int getBarCode(){return -1;}
}
