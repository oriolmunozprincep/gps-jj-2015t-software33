package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.utils.Repository;

import java.text.ParseException;
import java.util.Comparator;

/**
 * Created by sergi on 08/12/2015.
 */
public class UserController extends Repository<User> {
    public static Comparator<User> byPopularity = new Comparator<User>() {
        @Override
        public int compare(User u1, User u2) {
            return (u1.getUsername()).compareTo(u2.getUsername());
        }
    };

    public User findByName(final String username) {
        return find((p) -> p.getUsername().compareTo(username) == 0);
    }


    @Override
    protected void checkInsert(final User entity) throws RuntimeException {
        if(findByName(entity.getUsername())!=null)
            throw new IllegalArgumentException("Ja existeix un usuari amb aquest username");
    }

    public Worker newWorker(String username, String password) throws ParseException {
        Worker newUser = new Worker(username , password, newId());
        insert(newUser);
        return newUser;
    }

    public StoreManager newStoreManager(String username, String password) throws ParseException {
        StoreManager newUser = new StoreManager(username , password, newId());
        insert(newUser);
        return newUser;
    }

    public Administrator newAdministrator(String username, String password) throws ParseException {
        Administrator newUser = new Administrator(username , password, newId());
        insert(newUser);
        return newUser;
    }

    public User getUser (String username){
        return findByName(username);
    }
}
