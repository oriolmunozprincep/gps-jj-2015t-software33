package edu.upc.essi.gps.ecommerce;

import javafx.util.Pair;

import java.text.*;
import java.util.*;

public class TicketPrinter {

    private static final String HEADER = "Joguines i Joguets";
    private static final String lineBreak = "\n";
    private static final List<String> INDIVIDUAL_DISCOUNTS = Collections.singletonList("Percentual");
    NumberFormat moneyFormat = new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.FRANCE));

    public String printTicket(Sale sale, String shopName) {
        StringBuilder builder = new StringBuilder(HEADER).append(lineBreak)
                .append(shopName).append(lineBreak)
                .append(sale.getAddress()).append(lineBreak);

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        builder.append("Data: ").append(dateFormat.format(sale.getDate())).append(lineBreak);
        DateFormat hourFormat = new SimpleDateFormat("HH:mm");
        builder.append("Hora: ").append(hourFormat.format(sale.getDate())).append(lineBreak)
                .append("Venedor: ").append(sale.getSaleAssistantName()).append(lineBreak)
                .append("Número del TPV: ").append(sale.getPosNumber()).append(lineBreak)
                .append("ID Tiquet: ").append(sale.getId()).append(lineBreak)
                .append("Productes:").append(lineBreak);

        int i = 1;
        for (SaleLine line : sale.getLines()) {
            builder.append(i).append(")").append(line.getProductName()).append(lineBreak)
                    .append("Quantitat: ").append(line.getAmount()).append(" ")
                    .append("Preu sense IVA: ").append(moneyFormat.format(line.getTotalPrice())).append("€ ")
                    .append("Preu amb IVA: ").append(moneyFormat.format(line.getTotalPriceVat())).append("€").append(lineBreak);
            ++i;
            appendDiscountsOfLine(builder, sale, line);
        }

        builder.append(lineBreak).append("Desglossament d'IVA").append(lineBreak);
        Map<Integer, Breakdown> breakdowns = getBreakdowns(sale);
        for (Breakdown breakdown : breakdowns.values()) {
            builder.append("Base: ").append(moneyFormat.format(breakdown.getBase())).append("€")
                    .append(" IVA: ").append(breakdown.getVat())
                    .append(" Import: ").append(moneyFormat.format(breakdown.getTotal())).append("€")
                    .append(lineBreak);
        }

        int nonNullSize = 0;

        appendDiscountsOfSale(builder, sale.getAppliedDiscounts());

        if (sale.getIsLoyal()) {
            ++nonNullSize;
            appendLoyalDiscound(builder, sale.getMoneyDiscountedByLoyal());
        }
        appendVoucherOfSale(builder, sale.getAppliedVouchers());
        builder.append(lineBreak)
                .append("Preu total sense IVA: ").append(moneyFormat.format(sale.getVatlessTotal())).append("€").append(lineBreak)
                .append("Preu total amb IVA: ").append(moneyFormat.format(sale.getTotalWithoutDiscounts())).append("€").append(lineBreak);

        if (sale.getGivenMoney() != 0) {
            builder.append("Import aportat: ").append(moneyFormat.format(sale.getGivenMoney())).append("€").append(lineBreak);
            builder.append("Canvi a tornar: ").append(moneyFormat.format(sale.getGivenMoney() - sale.getTotal())).append("€").append(lineBreak);
        }

        for (Discount d : sale.getAppliedDiscounts()) {
            if (d != null) {
                ++nonNullSize;
            }
        }
        for (Voucher v : sale.getAppliedVouchers()) {
            if (v != null) {
                ++nonNullSize;
            }
        }
        if (nonNullSize > 0) {
            builder.append("Preu descomptat: ").append(moneyFormat.format(sale.getTotal())).append("€").append(lineBreak);
        }

        if (sale.getPaymentMethod() != null && sale.getPaymentMethod().equals("Targeta")) {
            builder.append("Pagat amb targeta").append(lineBreak);
        }

        return builder.toString();
    }

    private void appendLoyalDiscound(StringBuilder builder, double moneyDiscounted) {
        builder.append(lineBreak);
        builder.append("Altres descomptes aplicats:").append(lineBreak);
        builder.append("Descompte per client fidelitzat de ").append(moneyFormat.format(moneyDiscounted)).append("€").append(lineBreak);
    }

    private void appendVoucherOfSale(StringBuilder builder, List<Voucher> appliedVouchers) {
        boolean printedHeader = false;
        List<Discount> alreadyAdded = new ArrayList<>();
        for (Discount discount : appliedVouchers) {
            if (discount != null && !alreadyAdded.contains(discount)) {

                if (!printedHeader) {
                    builder.append(lineBreak);
                    builder.append("Altres descomptes aplicats:").append(lineBreak);
                    printedHeader = true;
                }

                builder.append(discount.getTicketDiscountLine()).append(lineBreak);
                alreadyAdded.add(discount);
            }
        }
    }

    private void appendDiscountsOfSale(StringBuilder builder, List<Discount> appliedDiscounts) {
        boolean printedHeader = false;
        List<Discount> alreadyAdded = new ArrayList<>();
        for (Discount discount : appliedDiscounts) {
            if (discount != null
                    && !INDIVIDUAL_DISCOUNTS.contains(discount.getDiscountType())
                    && !alreadyAdded.contains(discount)
                    && !(discount.getDiscountType().equals("NxM") && discount.getTicketDiscountLine().contains("en el producte"))) {

                if (!printedHeader) {
                    builder.append(lineBreak);
                    builder.append("Altres descomptes aplicats:").append(lineBreak);
                    printedHeader = true;
                }

                builder.append(discount.getTicketDiscountLine()).append(lineBreak);
                alreadyAdded.add(discount);
            }
        }
    }

    private void appendDiscountsOfLine(StringBuilder builder, Sale sale, SaleLine line) {
        List<Discount> discounts = sale.getAppliedDiscounts();
        for (Discount discount : discounts) {
            if (discount != null && discount.isProductInDiscount(line.getProductId()) && INDIVIDUAL_DISCOUNTS.contains(discount.getDiscountType())) {
                builder.append("Descompte aplicat: ")
                        .append(discount.getTicketDiscountLine()).append(lineBreak);
            }
            if (discount != null && discount.getDiscountType().equals("NxM") && discount.getTicketDiscountLine().contains("en el producte")) {
                builder.append("Descompte aplicat: ")
                        .append(discount.getTicketDiscountLine()).append(lineBreak);
            }
        }
    }

    private Map<Integer, Breakdown> getBreakdowns(Sale sale) {
        //Tree map so that they're ordered by lowest VAT base to highest
        Map<Integer, Breakdown> breakdowns = new TreeMap<>();

        for (SaleLine line : sale.getLines()) {
            Breakdown breakdown = breakdowns.get(line.getProductVat());
            if (breakdown == null) {
                breakdowns.put(line.getProductVat(),
                        new Breakdown(line.getTotalPrice(), line.getProductVat(), line.getTotalPriceVat()));
            } else {
                breakdown.setBase(breakdown.getBase() + line.getTotalPrice());
                breakdown.setTotal(breakdown.getTotal() + line.getTotalPriceVat());
            }
        }

        return breakdowns;
    }

    private class Breakdown {
        private double base;
        private final int vat;
        private double total;

        public Breakdown(double base, int vat, double total) {
            this.base = base;
            this.vat = vat;
            this.total = total;
        }

        public void setBase(double base) {
            this.base = base;
        }

        public void setTotal(double total) {
            this.total = total;
        }

        public double getBase() {
            return base;
        }

        public int getVat() {
            return vat;
        }

        public double getTotal() {
            return total;
        }
    }

}
