package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Alejandro Hidalgo on 02/12/2015.
 */
public class FixDiscount extends Discount{

    private int discountQuantity;

    public FixDiscount(long id, ArrayList<Product> products, Date initDate, Date finalDate, int discountQuantity) throws IllegalStateException {
        super(id, products, initDate, finalDate);
        if (discountQuantity <= 0)
            throw new IllegalStateException("El descompte a de ser major a 0");
        this.discountQuantity = discountQuantity;
    }

    public FixDiscount(long id, Date initDate, Date finalDate, String brand, ProductsService productsService, int discountQuantity) throws IllegalStateException {
        super(id, initDate, finalDate, brand, productsService);
        if (discountQuantity <= 0)
            throw new IllegalStateException("El descompte a de ser major a 0");
        this.discountQuantity = discountQuantity;
    }

    public int getDiscountQuantity(){
        return this.discountQuantity;
    }

    public String getDiscountType(){
        return "Fix";
    }

    @Override
    public String getTicketDiscountLine() {
        return "Descompte sobre la venda de " + discountQuantity + "€" ;
    }

}
