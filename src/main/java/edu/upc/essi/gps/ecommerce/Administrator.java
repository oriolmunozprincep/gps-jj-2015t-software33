package edu.upc.essi.gps.ecommerce;

import java.text.ParseException;

public class Administrator extends User {
    //També conegut com a cap de l'empresa
    public Administrator(String username, String password, long id) {
        super(username, password, id);
    }


    public void newWorkerUser(String name, String password, PosController pc){
        if(name.isEmpty())  throw new IllegalStateException("Has d'introduir un nom d'usuari");
        if(password.isEmpty())  throw new IllegalStateException("Has d'introduir una contrasenya");
        try {
            pc.getUserController().newWorker(name,password);
        } catch (ParseException e) {
            //  e.printStackTrace();
        }
    }

    public void newStoreManegerUser(String name, String password, PosController pc){
        if(name.isEmpty())  throw new IllegalStateException("Has d'introduir un nom d'usuari");
        if(password.isEmpty())  throw new IllegalStateException("Has d'introduir una contrasenya");
        try {
            pc.getUserController().newStoreManager(name, password);
        } catch (ParseException e) {
            //  e.printStackTrace();
        }
    }

    public void newAdministratorUser(String name, String password, PosController pc){
        if(name.isEmpty())  throw new IllegalStateException("Has d'introduir un nom d'usuari");
        if(password.isEmpty())  throw new IllegalStateException("Has d'introduir una contrasenya");
        try {
            pc.getUserController().newAdministrator(name,password);
        } catch (ParseException e) {
            //  e.printStackTrace();
        }
    }

    @Override
    public boolean isAllowedToSeeReports() {
        return true;
    }
}
