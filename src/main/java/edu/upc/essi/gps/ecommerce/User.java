package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sergi on 08/12/2015.
 */
public abstract class User implements Entity {
    private String username;
    private String password;
    private long id;

    public User (String username, String password, long id){
        this.username=username;
        this.password=password;
        this.id = id;
    }

    public void newWorkerUser(String name, String password, PosController pc){
        throw new IllegalStateException("Només els administradors o caps de botiga poden afegir nous usuaris");
    }

    public void newStoreManegerUser(String name, String password, PosController pc){
        throw new IllegalStateException("Només els administradors poden afegir nous caps de botiga");
    }

    public void newAdministratorUser(String name, String password, PosController pc) {
        throw new IllegalStateException("Només els administradors poden afegir nous administradors");
    }

    public String getUsername (){ return this.username; }

    public long getId (){ return  this.id; }

    public String getPassword() { return this.password; }

    public boolean login (String password){
        return (password.equals(this.password));
    }

    public void changePassword (String oldPassword , String newPassword , String newPAssword2){
        if(! login(oldPassword)) throw new IllegalStateException("Password antic incorrecte");
        else{
            if(! (newPassword.equals(newPAssword2))) throw new IllegalStateException("No coincideixen els dos passwords");
            else this.password=newPassword;
        }

    }

    public void startSale(Date date, PosController pc){
        if (pc.getCurrentWorkingDay()==null) throw new IllegalStateException("No s'ha iniciat un torn");
        if (pc.getCurrentSale() != null) throw new IllegalStateException("Aquest tpv ja té una venta iniciada");
        pc.setCurrentSale(new Sale(pc.getShopName(), pc.getAddress(), pc.getPosNumber(), this.username, date));
    }

    public void discardSale(PosController pc)  {
        if(pc.getCurrentSale().isPaid())
            pc.closeSale();
        else
            throw new IllegalStateException("No es podes descartar ventes pagades");
    }

    public void addProductByBarCode(int barCode, PosController pc) {
        if (pc.getCurrentWorkingDay()==null) throw new IllegalStateException("No s'ha iniciat un torn"); //falta feature
        if (pc.getCurrentSale() == null) pc.setCurrentSale(new Sale(pc.getShopName(),pc.getShopName() , pc.getPosNumber(), username, new Date()));//throw new IllegalStateException("No hi ha cap venta iniciada");
        Product p = pc.getProductsService().findByBarCode(barCode);
        addProductToCurrentSale(p, pc);
    }

    public void removeProductByBarCode(int barCode, PosController pc){
        if(pc.getCurrentWorkingDay() == null) throw new IllegalStateException("No s'ha iniciat torn");
        if(pc.getCurrentSale()==null) throw new IllegalStateException("No hi ha cap venta iniciada ");
        Product p = pc.getProductsService().findByBarCode(barCode);
        removeProductFromCurrentSale(p, pc);
    }

    public void addProductByLine(int line, PosController pc) {
        if (pc.getCurrentSale() == null) pc.setCurrentSale(new Sale(pc.getShopName(), pc.getAddress(), pc.getPosNumber(), username, new Date()));//throw new IllegalStateException("No hi ha cap venta iniciada");
        Product p = pc.getProductsService().listProductsByName().get(line - 1);
        addProductToCurrentSale(p, pc);
    }

    private void addProductToCurrentSale(Product p, PosController pc) {
        pc.getCurrentSale().addProduct(p);
        pc.bestDiscountsForSale();

    }

    private void removeProductFromCurrentSale(Product p, PosController pc){
        pc.getCurrentSale().removeProduct(p);
        pc.bestDiscountsForSale();
    }



    public double payment(String paymentMethod, Double givenMoney, PosController pc) {
        if (pc.getCurrentSale() == null) throw new IllegalStateException("No es pot cobrar una venta si no està  iniciada");
        else if(pc.getCurrentSale().isEmpty()) throw new IllegalStateException("No es pot cobrar una venta sense cap producte");

        if(paymentMethod.equals("Efectiu") || paymentMethod.equals("Targeta")) pc.getCurrentSale().setPaymentMethod(paymentMethod);
        else throw new IllegalStateException("No ÃƒÂ©s un metode de pagament correcte");
        pc.getCurrentSale().setGivenMoney(givenMoney);
        double change = givenMoney - pc.getCurrentSale().getTotal();
        if(pc.getCurrentSale().getPaymentMethod() != null && pc.getCurrentSale().getPaymentMethod().equals("Targeta")) change = 0;
        pc.closeSale();
        printTicket(pc);
        return change;
    }

    public String printTicket(PosController pc) {
        String tiquet;
        boolean b = false;
        TicketPrinter ticketPrinter = new TicketPrinter();
        if (pc.getCurrentWorkingDay() != null && pc.getCurrentWorkingDay().getSales().size() != 0) {
            Sale lastSale = pc.getCurrentWorkingDay().getSales().get(pc.getCurrentWorkingDay().getSales().size() - 1);
            tiquet = ticketPrinter.printTicket(lastSale, pc.getShopName());
        }
        else if (pc.getCurrentSale() != null) {
            tiquet = ticketPrinter.printTicket(pc.getCurrentSale(), pc.getShopName());
        }
        else {
            pc.setCurrentSale(new Sale(pc.getShopName(), pc.getAddress(), pc.getPosNumber(), username , new Date()));
            b = true;
            tiquet = ticketPrinter.printTicket(pc.getCurrentSale(), pc.getShopName());
        }
        if (b)
            pc.setCurrentSale(null);
        return tiquet;
    }

    public void setSaleAmountProduct(int barCode, int newAmount, PosController pc) {
        if(pc.getCurrentSale()==null) throw new IllegalStateException("No es pot modificar la quantitat d'un producte d'una venda si no hi ha una venda iniciada");
        Product p = pc.getProductsService().findByBarCode(barCode);
        pc.getCurrentSale().setAmountProduct(p, newAmount);
    }

    public void startOfWorkingDay(Date workingDayDate, double initialMoney, PosController pc) {

        if(initialMoney < 50)
            throw new IllegalStateException("No pots iniciar un torn amb menys de 50€");
        pc.setCurrentWorkingDay(pc.getWorkingDayController().newWorkingDay(workingDayDate, initialMoney, username, pc.getPosNumber()));
        pc.setRetry(3);
    }

    public String getExistingProductsMessage(PosController pc) {
        String welcomeMessage = "Els productes existents són:";
        List<Product> products = pc.getProductsService().listProductsByName();
        if (products.isEmpty()) throw new IllegalStateException("No hi ha productes a la botiga");;
        StringBuilder sb = new StringBuilder();
        sb.append(welcomeMessage).append("\n");
        for (Product p : products) {
            sb.append(p.getBarCode()).append(" - ")
                    .append(p.getName()).append("\n");
        }
        return sb.toString();
    }

    public void doBalance(double actualMoney, PosController pc)  {
        if(pc.getRetry() == 0) {
            pc.closeWorkingDay(actualMoney, "Maxim reintents");
            throw new IllegalStateException("No hi han mes reintents de quadrament");
        } else if(pc.getCurrentWorkingDay().closeDay(actualMoney)) {
            pc.setRetry(pc.getRetry()-1);
            throw new IllegalStateException("Quadrament incorrecte");
        } else{
            pc.closeWorkingDay(actualMoney, "Quadrament correcte");
        }
    }

    public long newPercentDiscount( ArrayList<Product> products, Date initDate, Date finalDate, double discountRate, PosController pc){
        return pc.getDiscountRepository().newPercentDiscount(products, initDate, finalDate, discountRate);
    }

    public long newPercentDiscount( Date initDate, Date finalDate, String brand, ProductsService productsService, double discountRate, PosController pc){
        return pc.getDiscountRepository().newPercentDiscount(initDate, finalDate, brand, productsService, discountRate);
    }

    public long newFixDiscount(ArrayList<Product> products, Date initDate, Date finalDate, int discountQuantity, PosController pc){
         return pc.getDiscountRepository().newFixDiscount(products, initDate, finalDate, discountQuantity);
    }

    public long newFixDiscount(Date initDate, Date finalDate, String brand, ProductsService productsService, int discountQuantity, PosController pc) {
        return pc.getDiscountRepository().newFixDiscount(initDate, finalDate, brand, productsService, discountQuantity);
    }

    public long newNxMDiscount(ArrayList<Product> products, Date initDate, Date finalDate, int n, int m, PosController pc){
        return pc.getDiscountRepository().newNxMDiscount(products, initDate, finalDate, n, m);
    }

    public long newNxMDiscount( Date initDate, Date finalDate, String brand, ProductsService productsService, int n, int m, PosController pc){
        return  pc.getDiscountRepository().newNxMDiscount(initDate, finalDate, brand, productsService, n, m);
    }

    public long newGiftDiscount(ArrayList<Product> products, Date initDate, Date finalDate, ArrayList<Product> gifts, PosController pc) {
        return pc.getDiscountRepository().newGiftDiscount(products, initDate, finalDate, gifts);
    }

    public long newGiftDiscount( Date initDate, Date finalDate, String brand, ProductsService productsService, ArrayList<Product> gifts, PosController pc){
        return pc.getDiscountRepository().newGiftDiscount(initDate, finalDate, brand, productsService, gifts);
    }

    public long newVoucher(Date initDate, Date finalDate, int discountQuantity, int minimumPay,int barCode, PosController pc){
        return  pc.getDiscountRepository().newVoucher(initDate, finalDate, discountQuantity, minimumPay, barCode);
    }


    public void setPriceToProduct(double price, int barCode, ProductsService productsService) {
        throw new IllegalStateException("Només els caps de botiga poden modificar els productes");
    }

    public void setVatPct(int iva, int barCode, ProductsService productsService) {
        throw new IllegalStateException("Només els caps de botiga poden modificar els productes");
    }

    public void setProductName(String name, int barCode, ProductsService productsService) {
        throw new IllegalStateException("Només els caps de botiga poden modificar els productes");
    }

    public void setLineAmount(int line, int amount, PosController posController) {
        if(posController.getCurrentSale()==null) throw new IllegalStateException("No es pot modificar la quantitat d'una linia de venda si no hi ha venda iniciada");
        posController.getCurrentSale().getLines().get(line - 1).setAmount(amount);
    }

    public void setGivenMoney(double money, PosController posController) {
        if(posController.getCurrentSale()==null) throw new IllegalStateException("No es poden modificar els diners rebuts d'una venda si no hi ha venda iniciada");
        posController.getCurrentSale().setGivenMoney(money);
    }

    public void addNewProduct(int barCode, ProductsService productsService) {
        throw new IllegalStateException("Només els caps de botiga poden afegir nous productes");
    }

    public void addNewProduct(String name, double price, int iva, int barCode, ProductsService productsService) {
        throw new IllegalStateException("Només els caps de botiga poden afegir nous productes");
    }

    public void newProductWithBrand(String name, String brand, double price, int iva, int barCode, ProductsService productsService, DiscountRepository discountRepository) {
        throw new IllegalStateException("Només els caps de botiga poden afegir nous productes");
    }

    public void newLoyalCostumer(String phoneNumber, String name, PosController pc){
        pc.getLoyalCostumerService().newLoyalCostumer(phoneNumber, name);
    }

    public void newLoyalCostumer(String phoneNumber, String name, int points, PosController pc){
        pc.getLoyalCostumerService().newLoyalCostumer(phoneNumber, name, points);
    }

    public boolean isAllowedToSeeReports() {
        return false;
    }
}
