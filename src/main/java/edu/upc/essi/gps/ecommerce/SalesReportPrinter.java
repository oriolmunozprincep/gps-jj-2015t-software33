package edu.upc.essi.gps.ecommerce;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

public class SalesReportPrinter {

    private static final String DAY_SEPARATOR = "------------------------------------------------------------";
    private static final String lineBreak = "\n";
    private static final String INDENT = "    ";
    private static final String DOUBLE_INDENT = INDENT+INDENT;
    private static final String TRIPLE_INDENT = DOUBLE_INDENT+INDENT;
    private static final SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
    private static final DecimalFormat moneyFormat = new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.FRANCE));

    public String print(List<WorkingDay> workingDays) {
        StringBuilder builder = new StringBuilder();

        Map<String, List<WorkingDay>> dateWorkingDay = groupByDay(workingDays);
        for (Map.Entry<String, List<WorkingDay>> me : dateWorkingDay.entrySet()) {
            int targeta = 0;
            int efectiu = 0;
            for (WorkingDay workingDay : me.getValue()) {
                for (Sale sale : workingDay.getSales()) {
                    if (sale.getPaymentMethod().equals("Targeta")) ++targeta;
                    else ++efectiu;
                }
            }
            builder.append("Dia ").append(me.getKey()).append(": ").append(efectiu).append( " pagaments en efectiu, ")
                    .append(targeta).append( " pagaments en targeta").append(lineBreak);

            for (WorkingDay day : me.getValue()) appendWorkingDay(builder, day);

            builder.append(DAY_SEPARATOR).append(lineBreak);
        }

        return builder.toString();
    }

    private Map<String, List<WorkingDay>> groupByDay(List<WorkingDay> workingDays) {
        Map<String, List<WorkingDay>> map = new TreeMap<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        for (WorkingDay workingDay : workingDays) {
            String day = dateFormat.format(workingDay.getWorkingDayDate());
            if (map.get(day) == null) {
                map.put(day, new ArrayList<>(Collections.singletonList(workingDay)));
            }
            else {
                map.get(day).add(workingDay);
            }
        }
        return map;
    }

    private void appendWorkingDay(StringBuilder builder, WorkingDay day) {
        for (Sale sale : day.getSales()) {
            builder.append(INDENT).append("Venda ").append(sale.getId()).append(":").append(lineBreak);
            builder.append(DOUBLE_INDENT).append("Venedor: ").append(sale.getSaleAssistantName()).append(lineBreak)
                    .append(DOUBLE_INDENT).append("Num TPV: ").append(day.getPosNumber()).append(lineBreak)
                    .append(DOUBLE_INDENT).append("Botiga: ").append(sale.getShop()).append(lineBreak)
                    .append(DOUBLE_INDENT).append("Adreça: ").append(sale.getAddress()).append(lineBreak);
            appendSale(builder, sale);
        }
    }

    private void appendSale(StringBuilder builder, Sale sale) {
        builder.append(DOUBLE_INDENT).append("Hora: ").append(hourFormat.format(sale.getDate())).append(lineBreak)
                .append(DOUBLE_INDENT).append("Productes:").append(lineBreak);

        for (SaleLine line : sale.getLines()) {
            builder.append(TRIPLE_INDENT).append(line.getProductName()).append(" x ").append(line.getAmount()).append(lineBreak);
        }

        builder.append(DOUBLE_INDENT).append("Total sense descomptes: ").append(moneyFormat.format(sale.getTotalWithoutDiscounts())).append("€").append(lineBreak)
                .append(DOUBLE_INDENT).append("Total amb descomptes: ").append(moneyFormat.format(sale.getTotal())).append("€").append(lineBreak)
                .append(DOUBLE_INDENT).append("Descomptes aplicats:").append(lineBreak);

        for (Discount d : sale.getAppliedDiscounts()) {
            builder.append(TRIPLE_INDENT).append(d.getReportLine()).append(lineBreak);
        }

        String estalviat = moneyFormat.format(sale.getTotalWithoutDiscounts()-sale.getTotal());
        builder.append(DOUBLE_INDENT).append("Estalviat per descomptes: ").append(estalviat).append("€").append(lineBreak)
                .append(DOUBLE_INDENT).append("Pagat amb: ").append(sale.getPaymentMethod());
        if (sale.getPaymentMethod().equals("Efectiu"))
            builder.append(" amb ").append(moneyFormat.format(sale.getGivenMoney())).append("€");
        builder.append(lineBreak);
    }
}
