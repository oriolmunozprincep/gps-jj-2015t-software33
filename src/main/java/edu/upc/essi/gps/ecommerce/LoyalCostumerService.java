package edu.upc.essi.gps.ecommerce;

import java.util.List;

public class LoyalCostumerService {

    private LoyalCostumerRepository loyalCostumerRepository;

    public LoyalCostumerService(LoyalCostumerRepository loyalCostumerRepository) {
        this.loyalCostumerRepository = loyalCostumerRepository;
    }

    private boolean existsPhoneNumber(String phoneNumber) {
        return loyalCostumerRepository.find((lc) -> lc.getPhoneNumber().equals(phoneNumber))!=null;
    }

    public void newLoyalCostumer(String phoneNumber, String name){
        if(existsPhoneNumber(phoneNumber))
            throw new IllegalStateException("Ja existeix un client fidelitzat amb aquest numero de telefon");
        long id = loyalCostumerRepository.newId();
        LoyalCostumer result = new LoyalCostumer(phoneNumber, name, id);
        loyalCostumerRepository.insert(result);
    }

    public void newLoyalCostumer(String phoneNumber, String name, int points){
        if(existsPhoneNumber(phoneNumber))
            throw new IllegalStateException("Ja existeix un client fidelitzat amb aquest numero de telefon");
        long id = loyalCostumerRepository.newId();
        LoyalCostumer result = new LoyalCostumer(phoneNumber, name, points, id);
        loyalCostumerRepository.insert(result);
    }

    public List<LoyalCostumer> listLoyalCostumers(){
        return loyalCostumerRepository.list();
    }

    public LoyalCostumer findByPhoneNumber(String phoneNumber) {
        return loyalCostumerRepository.findByPhoneNumber(phoneNumber);
    }
}
