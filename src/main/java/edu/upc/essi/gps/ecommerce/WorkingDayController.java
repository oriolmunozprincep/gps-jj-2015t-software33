package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.utils.Repository;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by sergi on 20/11/2015.
 */
public class WorkingDayController extends Repository<WorkingDay> {

    public static Comparator<WorkingDay> byPopularity = new Comparator<WorkingDay>() {
        @Override
        public int compare(WorkingDay wd1, WorkingDay wd2) {
            return (wd1.getWorkingDayDate()).compareTo(wd2.getWorkingDayDate());
        }
    };

    public WorkingDay findByDate(final Date date) {
        return find((p) -> p.getWorkingDayDate().compareTo(date) == 0);
    }

    public List<WorkingDay> findByDay(final Date date) {
        return list((p) -> date.getDay() == p.getWorkingDayDate().getDay() && date.getMonth() == p.getWorkingDayDate().getMonth() && date.getYear() == p.getWorkingDayDate().getYear());
    }

    @Override
    protected void checkInsert(WorkingDay entity) {
        if(findByDate(entity.getWorkingDayDate())!=null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfHours = new SimpleDateFormat("HH:mm");
            throw new IllegalArgumentException("Ja existeix un torn amb la data " + sdf.format(entity.getWorkingDayDate())+" y hora d' inici "+ sdfHours.format(entity.getWorkingDayDate()));
        }
    }

    public WorkingDay newWorkingDay(Date workingDayDate,double initialMoney, String currentSaleAssistantName, int postNumber) {
        WorkingDay newWorkingDay = new WorkingDay(workingDayDate , initialMoney, currentSaleAssistantName, postNumber);
        insert(newWorkingDay);
        return newWorkingDay;
    }


    public WorkingDay getWorkingDay (Date workingDayDate){
        return findByDate(workingDayDate);
    }
}

