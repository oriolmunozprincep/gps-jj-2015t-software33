package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.utils.Matchers;
import edu.upc.essi.gps.utils.Repository;
import org.omg.CORBA.portable.ValueOutputStream;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Alejandro Hidalgo on 02/12/2015.
 */
public class DiscountRepository extends Repository<Discount> {

    private long nextid = 0;


    private long nextId() {
        return ++nextid;
    }


    @Override
    protected void checkInsert(Discount entity) throws RuntimeException {
        if(findById(entity.getId()) != null)
            throw new IllegalArgumentException("Ja existeix un descompte amb id " + entity.getId());
    }

    public Discount findById(final long id) {
        return find((d) -> d.getId() == id);
    }

    public Voucher findVoucherByBarCode(final int barCode){
        return (Voucher)find((p) -> p.getBarCode() == barCode);
    }

    public long newPercentDiscount(ArrayList<Product> products, Date initDate, Date finalDate, double discountRate){
        PercentDiscount pd = new PercentDiscount(nextId(), products, initDate, finalDate, discountRate);
        insert(pd);
        return nextid;
    }

    public long newPercentDiscount(Date initDate, Date finalDate, String brand, ProductsService productsService, double discountRate){
        PercentDiscount pd = new PercentDiscount(nextId(), initDate, finalDate, brand, productsService, discountRate);
        insert(pd);
        return nextid;
    }

    public long newFixDiscount(ArrayList<Product> products, Date initDate, Date finalDate, int discountQuantity){
        FixDiscount fd = new FixDiscount(nextId(), products, initDate, finalDate, discountQuantity);
        insert(fd);
        return nextid;
    }

    public long newFixDiscount(Date initDate, Date finalDate, String brand, ProductsService productsService, int discountQuantity){
        FixDiscount fd = new FixDiscount(nextId(), initDate, finalDate, brand, productsService, discountQuantity);
        insert(fd);
        return nextid;
    }

    public long newNxMDiscount( ArrayList<Product> products, Date initDate, Date finalDate, int n, int m){
        NxMDiscount nmD = new NxMDiscount(nextId(), products, initDate, finalDate, n, m);
        insert(nmD);
        return nextid;
    }

    public long newNxMDiscount(Date initDate, Date finalDate, String brand, ProductsService productsService, int n, int m){
        NxMDiscount nmD = new NxMDiscount(nextId(), initDate, finalDate, brand, productsService, n, m);
        insert(nmD);
        return nextid;
    }

    public long newGiftDiscount(ArrayList<Product> products, Date initDate, Date finalDate, ArrayList<Product> gifts){
        GiftDiscount gf = new GiftDiscount(nextId(), products, initDate, finalDate, gifts);
        insert(gf);
        return nextid;
    }

    public long newGiftDiscount(Date initDate, Date finalDate, String brand, ProductsService productsService, ArrayList<Product> gifts){
        GiftDiscount gf = new GiftDiscount(nextId(), initDate, finalDate, brand, productsService, gifts);
        insert(gf);
        return nextid;
    }

    private boolean existsVoucherWithBarCode(int barCode) {
       return find((p) -> p.getBarCode() == barCode)!=null;
    }

    public long newVoucher(Date initDate, Date finalDate, int discountQuantity, int minimumPay, int barCode) {
        if(existsVoucherWithBarCode(barCode)) throw new IllegalStateException("Ja hi ha un val de descompte amb aquest codi de barres");
        Voucher v = new Voucher(nextId(),initDate,finalDate,discountQuantity,minimumPay,barCode);
        insert(v);
        return nextid;
    }

    public List<PercentDiscount> getAllPercentDiscount(){
        List<Discount> list = list();
        ArrayList<PercentDiscount> al = new ArrayList<>();
        for(Discount d : list){
            if(d.getDiscountType().equals("Percentual"))
                al.add((PercentDiscount) d);
        }
        return al;
    }

    public List<FixDiscount> getAllFixDiscount(){
        List<Discount> list = list();
        ArrayList<FixDiscount> al = new ArrayList<>();
        for(Discount d : list){
            if(d.getDiscountType().equals("Fix"))
                al.add((FixDiscount) d);
        }
        return al;
    }

    public List<NxMDiscount> getAllNxMDiscount(){
        List<Discount> list = list();
        ArrayList<NxMDiscount> al = new ArrayList<>();
        for(Discount d : list){
            if(d.getDiscountType().equals("NxM"))
                al.add((NxMDiscount) d);
        }
        return al;
    }

    public List<GiftDiscount> getAllGiftDiscount(){
        List<Discount> list = list();
        ArrayList<GiftDiscount> al = new ArrayList<>();
        for(Discount d : list){
            if(d.getDiscountType().equals("Regal"))
                al.add((GiftDiscount) d);
        }
        return al;
    }

    public List<Voucher> getAllVoucher(){
        List<Discount> list = list();
        ArrayList<Voucher> al = new ArrayList<>();
        for(Discount d : list){
            if(d.getDiscountType().equals("Voucher"))
                al.add((Voucher) d);
        }
        return al;
    }

    public Voucher findVoucher(Date dateIni, Date dateFi, int discountQuantity, int minimumPay){
        List<Voucher> lv = getAllVoucher();
      //  System.out.println()
        for (Voucher v : lv){
            if (v.getDiscountQuantity() == discountQuantity &&
                    v.getMinimumPay() == minimumPay &&
                    v.getInitDate().equals(dateIni) &&
                    v.getFinalDate().equals(dateFi))
                return v;
        }
        return null;
    }

    public Voucher findVoucher(int discountQuantity, int minimumPay){
        List<Voucher> lv = getAllVoucher();
        for (Voucher v : lv){
            if (v.getDiscountQuantity() == discountQuantity &&
                    v.getMinimumPay() == minimumPay)
                return v;
        }
        return null;
    }
}
