package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.utils.Comparators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ProductsService {

    private ProductsRepository productsRepository;

    public ProductsService(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    private boolean existsBarCode(int barCode) {
        return productsRepository.find((p) -> p.getBarCode() == barCode)!=null;
    }

    public void newProduct(String name, double price, int vatPct, int barCode){
        if(existsBarCode(barCode)) throw new IllegalStateException("Ja existeix un producte amb aquest codi de barres");
        long id = productsRepository.newId();
        Product result = new Product(id,name, price, vatPct, barCode);
        productsRepository.insert(result);
    }

    public void newProduct(int barCode) {
        if(existsBarCode(barCode)) throw new IllegalStateException("Ja existeix un producte amb aquest codi de barres");
        long id = productsRepository.newId();
        Product relust = new Product(id, barCode);
        productsRepository.insert(relust);
    }

    public void newProductWithBrand(String productName, double price, int vatPct, int barCode, String brandName) {
        if(existsBarCode(barCode)) throw new IllegalStateException("Ja existeix un producte amb aquest codi de barres");
        long id = productsRepository.newId();
        Product result = new Product(id,productName, price, vatPct, barCode, brandName);
        productsRepository.insert(result);
    }

    public List<Product> listProducts(){
        return productsRepository.list();
    }

    public List<Product> listProductsByName(){
        return productsRepository.list(Comparators.byName);
    }

    public Product findByName(String productName) {
        return productsRepository.findByName(productName);
    }

    public Product findByBarCode(int barCode) {
        return productsRepository.findByBarCode(barCode);
    }

    public ArrayList<Product> findByBrand(String brand){
        ArrayList<Product> products = new ArrayList<>();
        List<Product> list = productsRepository.list();
        for(Product p : list){
            if(p.hasBrand() && p.getBrand().equals(brand))
                products.add(p);
        }
        return products;

    }

}
