package edu.upc.essi.gps.ecommerce;

import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

class SaleLine{
    private long productId;
    private String productName;
    private double unitPrice;
    //even if the product usually maintains vat, it may change, so we store it at the moment of the sale
    private int vat;
    private int amount;
    private Integer barCode;

    public SaleLine(Product product, int amount) {
        this.productId = product.getId();
        this.productName = product.getName();
        this.unitPrice = product.getPrice();
        this.amount = amount;
        this.vat = product.getVatPct();
        this.barCode= product.getBarCode();
    }

    public long getProductId() {return productId;}

    public String getProductName() {  return productName;}

    public double getUnitPrice() { return unitPrice;}

    public double getUnitPriceVat() {
        return unitPrice+ unitPrice*((double)vat/100.0);
    }

    public int getAmount() {return amount; }

    public double getTotalPriceVat() {
        return (unitPrice * amount)+(unitPrice * amount)*((double)vat/100.0);
    }

    public double getTotalPrice() {
        return unitPrice * amount;
    }

    public void incrementAmount(){
        ++amount;
    }

    public void decrementAmount(){
        --amount;
    }

    public int getProductVat(){return vat;}

    public void setAmount(int prodAmount) { amount = prodAmount; }

    public Integer getProductBarCode(){return barCode;}

}

public class Sale {
    private static long NEXT_ID = 1;

    private long id;
    private final String shop;
    private final String address;
    private final int posNumber;
    private String saleAssistantName;
    private final List<SaleLine> lines = new LinkedList<>();
    private String paymentMethod;
    private double givenMoney;
    private Date saleDate;
    private boolean paid;
    private List<Discount> appliedDiscounts = new ArrayList<>();
    private ArrayList<Pair<ArrayList<Product>,NxMDiscount>> nxmDiscountsMultiples = new ArrayList<>();
    private List<Voucher> appliedVouchers = new ArrayList<>();
    private boolean isLoyal = false;
    private double moneyDiscountedByLoyal = 0;

    public void setSpecial( ArrayList<Pair<ArrayList<Product>,NxMDiscount>> nxmDiscountsMultiples){
        this.nxmDiscountsMultiples=nxmDiscountsMultiples;
    }
    public void addProduct(Product p) {
        boolean exists = false;
        long productId = p.getId();
        int i;
        for(i = 0; i < lines.size() && !exists; i++) {
            long id = lines.get(i).getProductId();
            if(id==productId) exists = true;
        }
       if(exists) {
           lines.get(i - 1).incrementAmount();
       }
       else lines.add(new SaleLine(p,1));
    }

    public void removeProduct(Product p) {
        boolean exists = false;
        long productId = p.getId();
        int i;
        for(i = 0; i < lines.size() && !exists; ++i){
            long id = lines.get(i).getProductId();
            if(id == productId) exists = true;
        }
        if(exists) {
            lines.get(i - 1).decrementAmount();
            if(lines.get(i-1).getAmount() <= 0)
                lines.remove(i-1);
        }
    }

    public Sale(String shop, String address, int posNumber, String saleAssistantName, Date date) {
        this.id = NEXT_ID++;
        this.shop = shop;
        this.address = address;
        this.posNumber = posNumber;
        this.saleAssistantName = saleAssistantName;
        this.saleDate = date;
        paid = false;
    }

    public void pay () {
        this.paid = true;
    }

    public boolean isPaid () {
        return paid;
    }

    public long getId() {
        return id;
    }

    public String getShop() {
        return shop;
    }

    public String getAddress() {
        return address;
    }

    public int getPosNumber() {
        return posNumber;
    }

    public String getSaleAssistantName() {
        return saleAssistantName;
    }

    public Date getDate(){return saleDate;}

    public List<SaleLine> getLines() {
        return Collections.unmodifiableList(lines);
    }

    //returns price with Vat
    public double getTotalPriceVatAndBase(double taxBase, double totalVat){
        double res = taxBase=0.0;
        for(SaleLine l : lines){
            taxBase+=l.getTotalPrice();
            res += l.getTotalPriceVat();
        }
        totalVat=res-taxBase;
        return res;
    }

    //With VAT
    public double getTotal() {
        double res = 0;
        int i = 0;
        if (appliedDiscounts.size() > 0 && appliedDiscounts.get(0) != null) {
            if ((i <= appliedDiscounts.size() - 1) && appliedDiscounts.get(i).getDiscountType().equals("Fix")) {
                res -= ((FixDiscount) appliedDiscounts.get(i)).getDiscountQuantity();
                i++;
            }
            for (SaleLine l : lines) {
                if (i > appliedDiscounts.size() - 1) res += l.getTotalPriceVat();
                else if (appliedDiscounts.get(i) == null) res += l.getTotalPriceVat();
                else res += appliedDiscounts.get(i).priceWithDiscount(l);
                ++i;
            }
        }
        else {
            for (SaleLine l : lines) {
                res += l.getTotalPriceVat();
            }
        }
        for (Voucher v : appliedVouchers){
            res -= v.getDiscountQuantity();
        }
        if (isLoyal){ System.out.println("descuento cosas que no son");
            res -= moneyDiscountedByLoyal;
        }
        if(nxmDiscountsMultiples.size()>0){
            Pair<ArrayList<Product>,NxMDiscount> aux = nxmDiscountsMultiples.get(0);
            double disc =aux.getValue().discountAmmount(aux.getKey());
            res-=disc;
        }
        return res;
    }

    public double getTotalWithoutDiscounts() {
        double res = 0;
        for(SaleLine l : lines) {
            res += l.getTotalPriceVat();
        }
        return res;
    }

    public double getVatlessTotal() {
        double res = 0;
        for (SaleLine l : lines) {
            res += l.getTotalPrice();
        }
        return res;
    }

    public boolean isEmpty() {
        return lines.isEmpty();
    }

    public void setAmountProduct(Product p, int newAmount) {
        long productId = p.getId();
        boolean exists = false;
        int i;
        for(i = 0; i < lines.size() && !exists; i++) {
            long id = lines.get(i).getProductId();
            if(id==productId) exists = true;
        }
        if(exists) lines.get(i-1).setAmount(newAmount);
        else {
            lines.add(new SaleLine(p,1));
            lines.get(i).setAmount(newAmount);
        }
    }

    public void setPaymentMethod(String method){
        paymentMethod = method;
    }

    public String getPaymentMethod() { return paymentMethod; }

    public void setGivenMoney(double money){
        givenMoney = money;
    }

    public double getGivenMoney() { return givenMoney; }

    public void setId(long id) {
        this.id = id;
    }

    public List<Discount> getAppliedDiscounts() {
        List<Discount> discounts = new LinkedList<>(appliedDiscounts);
        for (Pair<ArrayList<Product>, NxMDiscount> nxmDiscountsMultiple : nxmDiscountsMultiples) {
            discounts.add(nxmDiscountsMultiple.getValue());
        }
        return discounts.stream().filter(d -> d != null).collect(Collectors.toList());
    }

    public void setAppliedDiscounts(List<Discount> appliedDiscounts) {
        this.appliedDiscounts = appliedDiscounts;
    }

    public List<Voucher> getAppliedVouchers() { return appliedVouchers; }

    public void setAppliedVouchers(List<Voucher> appliedVouchers) { this.appliedVouchers = appliedVouchers; }

    public double getMoneyDiscountedByLoyal() { return moneyDiscountedByLoyal; }

    public void setMoneyDiscountedByLoyal(double moneyDiscountedByLoyal) {
        isLoyal = true;
        this.moneyDiscountedByLoyal = moneyDiscountedByLoyal;
    }

    public boolean getIsLoyal() { return isLoyal; }

    public ArrayList<Pair<ArrayList<Product>, NxMDiscount>> getNxmDiscountsMultiples() {
        return nxmDiscountsMultiples;
    }

    public void setSaleAssistantName(String saleAssistantName) {
        this.saleAssistantName = saleAssistantName;
    }
}
