package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.utils.Repository;


public class LoyalCostumerRepository extends Repository<LoyalCostumer> {

    public LoyalCostumer findById(final long id) {
        return find((lc) -> lc.getId() == id);
    }

    public LoyalCostumer findByPhoneNumber(final String phoneNumber){
        return find((lc) -> lc.getPhoneNumber().equals(phoneNumber));
    }

    @Override
    protected void checkInsert(final LoyalCostumer entity) throws RuntimeException {
        if(findByPhoneNumber(entity.getPhoneNumber())!=null)
            throw new IllegalArgumentException("Ja existeix un client fidelitzat amb aquest telefon");
    }

}
