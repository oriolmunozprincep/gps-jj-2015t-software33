package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.Entity;
import edu.upc.essi.gps.domain.HasName;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created by sergi on 20/11/2015.
 */

public class WorkingDay implements Entity, HasName{
    private Date workingDayDate;
    private double initialMoney;
    private double finalMoney;
    private double imbalance;
    private String msg;
    private boolean finished;
    private ArrayList<Sale> workingDaySales;
    private String currentSaleAssistantName;
    private int postNumber;

    public WorkingDay(Date workingDayDate, double initialMoney, String currentSaleAssistantName, int postNumber){
        this.finished = false;
        this.workingDaySales=new ArrayList<Sale>();
        this.workingDayDate = workingDayDate;
        this.initialMoney=initialMoney;
        this.currentSaleAssistantName=currentSaleAssistantName;
        this.postNumber=postNumber;
    }

    public ArrayList<Sale> getSales (){ return this.workingDaySales;}

    public double getInitialMoney (){ return this.initialMoney;}

    public void addSale (Sale sale){
        workingDaySales.add(sale);
    }

    public Date getWorkingDayDate(){
        return this.workingDayDate;
    }

    @Override
    public long getId() {
        return workingDayDate.getTime();
    }

    @Override
    public String getName() {
        return null;
    }

    public boolean closeDay(double actualMoney) {
        closeDay(actualMoney, null);
        return (actualMoney != expectedMoney());
    }

    public void closeDay(double actualMoney, String msg){
        int aux = (int)(actualMoney*100) - (int)(expectedMoney()*100);
        this.imbalance = aux/(double)100;
        if(imbalance < 0)
            imbalance *= -1;
        this.msg = msg;
        finished = true;
    }

    public double getImbalance() {
        if(finished) return this.imbalance;
        else throw new IllegalStateException("Encara no s'ha acabat el torn");
    }

    public boolean getFinished() {
        return finished;
    }

    public String getSaleAssistantName() {
        return this.currentSaleAssistantName;
    }

    public int getPosNumber() {
        return this.postNumber;
    }


    public String getImbalanceMessage() {
        if(finished) return this.msg;
        else throw new IllegalStateException("Encara no s'ha acabat el torn");
    }

    private double expectedMoney(){
        double em = 0;
        for(Sale s: workingDaySales){
            em += s.getTotal();
        }
        return em + initialMoney;
    }
}
