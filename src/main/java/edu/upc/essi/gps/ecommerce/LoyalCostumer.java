package edu.upc.essi.gps.ecommerce;

import edu.upc.essi.gps.domain.Entity;

public class LoyalCostumer implements Entity {

    private String phoneNumber;
    private String name;
    private int points;
    private long id;

    public LoyalCostumer(String phoneNumber, String name, long id) {
        this.phoneNumber = phoneNumber;
        this.name = name;
        points = 0;
        this.id = id;
    }

    public LoyalCostumer(String phoneNumber, String name, int points, long id) {
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.points = points;
        this.id = id;
    }

    public long getId(){ return id; }

    public String getPhoneNumber() { return phoneNumber; }

    public String getName() { return name; }

    public int getPoints() { return points; }

    public void setPoints(double moneySpent) { this.points = (int)moneySpent*100; }

}
