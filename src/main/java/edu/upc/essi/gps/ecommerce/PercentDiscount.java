package edu.upc.essi.gps.ecommerce;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Alejandro Hidalgo on 02/12/2015.
 */
public class PercentDiscount extends Discount {

    private double discountRate;

    public PercentDiscount(long id, ArrayList<Product> products, Date initDate, Date finalDate, double discountRate) throws IllegalStateException {
        super(id, products, initDate, finalDate);
        if (discountRate < 0 || discountRate > 1)
            throw new IllegalStateException("El descompte a de ser un valor entre 0 i 1");
        this.discountRate = discountRate;
    }

    public PercentDiscount(long id, Date initDate, Date finalDate, String brand, ProductsService productsService, double discountRate) throws IllegalStateException {
        super(id, initDate, finalDate, brand, productsService);
        if (discountRate < 0 || discountRate > 1)
            throw new IllegalStateException("El descompte a de ser un valor entre 0 i 1");
        this.discountRate = discountRate;
    }

    public double getDiscountRate(){
        return this.discountRate;
    }

    public String getDiscountType(){
        return "Percentual";
    }

    @Override
    public String getTicketDiscountLine() {
        int discount = (int) (discountRate*100);
        return discount+"% de descompte";
    }

    @Override
    public String getReportLine() {
        return getTicketDiscountLine() + " en " + allProductsNames();
    }

    public double priceWithDiscount (SaleLine products){
        isProductInDiscount(products.getProductId());
        double disc = discountRate * products.getTotalPriceVat();
        return (products.getTotalPriceVat() - disc);
    }
}
