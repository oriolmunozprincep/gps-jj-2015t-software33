package edu.upc.essi.gps.ecommerce;

import cucumber.api.PendingException;
import cucumber.api.java.ca.Aleshores;
import cucumber.api.java.ca.Donat;
import cucumber.api.java.ca.I;
import cucumber.api.java.ca.Quan;
import gherkin.deps.com.google.gson.Gson;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class StepDefinitions {

    private static final Gson gson = new Gson();

    private Exception exception;
    private PosController posController;
    private double change;
    private String ticket;
    private String report;
    private Sale temporalsale;
    private String jefe;
    private String contraJefe;
    private Date date;
    private long lastDiscount = 0;

    public void tryCatch(Runnable r){
        try {
            r.run();
            this.exception = null;
        } catch (Exception e){
            this.exception = e;
        }
    }

    @Aleshores("^obtinc un error que diu: \"([^\"]*)\"$")
    public void checkErrorMessage(String msg) throws Throwable {
        assertNotNull(this.exception);
        assertEquals(msg, this.exception.getMessage());
    }

    @Donat("^que estem al tpv número (\\d+) de la botiga \"([^\"]*)\" amb adreça \"([^\"]*)\"$")
    public void setupPos(int posNumber, String shop, String address) throws Throwable {
        this.posController = new PosController(shop, address, posNumber);
    }

    @Aleshores("^el tpv està en ús per en \"([^\"]*)\"$")
    public void checkCurrentSaleAssistantName(String saleAssistantName) throws Throwable {
        assertEquals(saleAssistantName, this.posController.getCurrentSaleAssistantName());
    }

    @Aleshores("^la venta actual és de'n \"([^\"]*)\" al tpv (\\d+) de la botiga \"([^\"]*)\"$")
    public void checkCurrentSaleData(String saleAssistant, int posNumber, String shop) throws Throwable {
        Sale s = this.posController.getCurrentSale();
        assertNotNull(s);
        assertEquals(shop, s.getShop());
        assertEquals(posNumber, s.getPosNumber());
        assertEquals(saleAssistant, s.getSaleAssistantName());
    }



    @Donat("^que en \"([^\"]*)\" ha iniciat la sessio al tpv amb contrasenya \"([^\"]*)\"$")
    public void que_en_ha_iniciat_la_sessio_al_tpv_amb_contrasenya(String saleAssistantName, String pass) throws Throwable {
        tryCatch(() -> this.posController.login(saleAssistantName, pass));
    }

    @Quan("^inicio una nova venta$")
    public void tryStartSale(){
        tryCatch(() -> this.posController.startSale(date));
        temporalsale = posController.getCurrentSale();
    }

    @Donat("^que hi ha una venta iniciada amb data \"([^\"]*)\" i hora \"([^\"]*)\"")
    public void saleSartedWithDate(String dateStr, String hourStr) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = dateFormat.parse(dateStr + " " + hourStr);
        this.posController.startSale(date);
        this.posController.getCurrentSale().setId(1L);      // forçat per la feature del tiquet
    }

    @Donat("^un producte amb nom \"([^\"]*)\", preu (.+)€, iva (\\d+)% i codi de barres (\\d+)$")
    public void productCreated(String productName, double price, int vatPct, int barCode) throws Throwable {
        tryCatch(() -> this.posController.addNewProduct(productName, price, vatPct, barCode));
    }

    @Quan("^afegeixo el producte de codi de barres (\\d+) a la venta$")
    public void addProductByBarCode(int barCode) throws Throwable {
        tryCatch(() -> this.posController.addProductByBarCode(barCode));
    }

    @Aleshores("^la venta té (\\d+) (?:línia|línies)$")
    public void la_venta_té_n_linies(int expectedNumberOfLines) throws Throwable {
        assertEquals(expectedNumberOfLines, this.posController.getCurrentSale().getLines().size());
    }

    @Aleshores("^línia de venta (\\d+) és de (\\d+) unitats de \"([^\"]*)\" a (.+)€ cada una per un total de (.+)€$")
    public void línia_de_venta_és_de_unitats_de_a_€_cada_una_per_un_total_de_€(int lineNumber, int units, String productName, double unitPrice, double totalPrice) throws Throwable {
        SaleLine sl = this.posController.getCurrentSale().getLines().get(lineNumber - 1);
        assertEquals(units, sl.getAmount());
        assertEquals(unitPrice,sl.getUnitPriceVat(),0.001);
        assertEquals(totalPrice,sl.getTotalPriceVat(),0.001);
        assertEquals(productName, sl.getProductName());
    }

    @Aleshores("^el total de la venta actual és de (.+)€$")
    public void el_total_de_la_venta_actual_és_de_€(double saleTotal) throws Throwable {
        assertEquals(saleTotal, Math.floor(this.posController.getCurrentSale().getTotal() * 100 + 0.5) / 100, 0.001);
    }

    @Aleshores("^la pantalla del client del tpv mostra$")
    public void la_pantalla_del_client_del_tpv_mostra(String msg) throws Throwable {
        assertEquals(msg, this.posController.getCustomerScreenMessages());
    }

    @Aleshores("^el tpv m'indica que el canvi a retornar és de (.+)€$")
    public void checkChange(double expectedChange) throws Throwable {
        assertEquals(expectedChange, change, 0.001);
    }
    @Quan("^inicio un nou torn amb (.+)€$")
    public void iniciJornada (double inicialEffecitve) throws Throwable {
            Date dateOfWorkingDay = new Date();
            tryCatch(() -> this.posController.startOfWorkingDay(dateOfWorkingDay, inicialEffecitve));
    }

    @Aleshores("^El nou torn iniciat te una caixa inicial del (.+)€$")
    public void checkCreacioJornada(double initialMoney) throws Throwable {
        assertEquals(initialMoney, this.posController.getCurrentWorkingDay().getInitialMoney(), 0);
    }

    @Aleshores("^el dia de la venda és l'actual$")
    public void el_dia_de_la_venda_és_l_actual() throws Throwable {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        assertEquals(dateFormat.format(new Date()), dateFormat.format(this.posController.getCurrentSale().getDate()));
    }

    @Aleshores("^l'iva del producte de la línia (\\d+) és del (\\d+)%$")
    public void l_iva_del_producte_de_la_línia_és_del_(int saleLine, int ivaProduct) throws Throwable {
        assertEquals(this.posController.getCurrentSale().getLines().get(saleLine - 1).getProductVat(), ivaProduct);
    }

    @Quan("^modifico la quantitat de la línia (\\d+) a (\\d+) unitats$")
    public void modifico_la_quantitat_de_la_línia_a_unitats(int line, int amount) throws Throwable {
        tryCatch(() -> this.posController.setLineAmount(line, amount));
    }

    @Quan("^modifico la quantitat del producte amb codi de barres (\\d+) a (\\d+) unitats$")
    public void modifico_la_quantitat_del_producte_amb_codi_de_barres_a_unitats(int barCode, int newAmount) throws Throwable {
        tryCatch(() -> this.posController.setSaleAmountProduct(barCode, newAmount));
    }

    /** Tiquet **/
    @Quan ("^vull generar un tiquet$")
    public void vull_generar_un_tiquet() throws Exception {
        tryCatch(() -> ticket = this.posController.printTicket());
    }

    @Aleshores("^es genera un tiquet amb tota la informació de la venta igual a \"([^\"]*)\"$")
    public void es_genera_un_tiquet_amb_tota_la_informació_de_la_venta(String ticketFileName) throws Exception {
        byte[] ticket1s = Files.readAllBytes(Paths.get(getClass().getResource("/tickets/"+ticketFileName).toURI()));
        String ticket = new String(ticket1s, "UTF-8");
        assertEquals(ticket, this.ticket);
    }
    /**Fi tiquet**/

    /** Canvi **/
    @Donat("^que el client aporta (.+)€$")
    public void que_el_client_aporta_€(double money){
        tryCatch(() -> this.posController.setGivenMoney(money));
    }


    @Aleshores("^el canvi a tornar de la venta ha sigut (.+)€$")
    public void el_canvi_a_tornar_de_la_venta_ha_sigut_€(double expectedChange){
        assertEquals(expectedChange, change, 0.001);
    }
    /**Fi canvi**/

    @Quan("^que vull canviar el nom de la botiga a \"([^\"]*)\"$")
    public void que_vull_canviar_el_nom_de_la_botiga_a(String newName) throws Throwable {
        this.posController.setShopName(newName);
    }

    @Aleshores("^el nom que sortira al tiquet sera \"([^\"]*)\"$")
    public void el_nom_que_sortira_al_tiquet_sera(String newName) throws Throwable {
        assertTrue("El nom de la botiga no s'ha canviat", this.posController.printTicket().contains(newName));
    }

    @Donat("^que vull canviar el numero del TPV al (\\d+)$")
    public void que_vull_canviar_el_numero_del_TPV_al(int newPosNumber) throws Throwable {
        this.posController.setPosNumber(newPosNumber);
    }

    @Aleshores("^el numero de TPV que sortira al tiquet sera el (\\d+)$")
    public void el_numero_de_TPV_que_sortira_al_tiquet_sera_el(int newPosNumber) throws Throwable {
        assertTrue("El numero del TPV no s'ha canviat", this.posController.printTicket().contains("Número del TPV: " + Integer.toString(newPosNumber)));
    }

    @Donat("^que el tpv mostra la llista de productes \"([^\"]*)\"$")
    public void que_el_tpv_mostra_la_llista_de_productes(String ProductList) throws Throwable {
        byte[] llista = Files.readAllBytes(Paths.get(getClass().getResource("/"+ProductList).toURI()));
        String ticket = new String(llista, "UTF-8");
        final String[] res = new String[1];
        tryCatch(() -> res[0] = this.posController.getExistingProductsMessage());
        assertEquals(ticket, res[0]);
    }

    @Quan("^afegeixo el producte de la linia (\\d+)$")
    public void afegeixo_el_producte_de_la_linia(int line) throws Throwable {
        tryCatch(() -> this.posController.addProductByLine(line));
    }

    @Donat("^que s'ha iniciat un torn amb (.+)€$")
    public void que_s_ha_iniciat_la_jornada_amb_€(double initialMoney) throws Throwable {
        date = new Date();
        tryCatch(() -> this.posController.startOfWorkingDay(date, initialMoney));
    }

    @Quan("^tanco el torn indicant que hi han (.+)€ a caixa$")
    public void vull_tancar_la_jornada_indicant_que_hi_han_€_a_caixa(double actualMoney) throws Throwable {
        tryCatch(() -> this.posController.doBalance(actualMoney));
    }

    @Quan("^es va tancar la jornada indicant que hi han (.+)€ a caixa$")
    public void es_va_tancar_la_jornada_indicant_que_hi_han_€_a_caixa(double actualMoney) throws Throwable {
        tryCatch(() -> this.posController.doBalance(actualMoney));
    }

    @Aleshores("^no sortira cap missatge d'error$")
    public void checkNoError() throws Throwable {
        assertNull(this.exception);
    }

    @Aleshores("^llavors fare recompte i veure que hi han (.+)€ a caixa$")
    public void fare_recompte_i_veure_que_hi_han_€_a_caixa(double actualMoney) throws Throwable {
        tryCatch(() -> this.posController.doBalance(actualMoney));
    }

    @I("^tanco la venta$")
    public void tanco_la_venta() throws Throwable {
        tryCatch(() -> this.posController.closeSale());
    }

    @Aleshores("^la venta es en \"([^\"]*)\" i està enregistrada$")
    public void la_venta_es_en_i_està_enregistrada(String arg1) throws Throwable {
        assertTrue(temporalsale.getPaymentMethod().equals(arg1));
        assertTrue(this.posController.getCurrentWorkingDay().getSales().contains(temporalsale));
    }

    @Quan("^intento tornar a iniciar el dia \"([^\"]*)\" amb (.+)€$")
        public void intento_s_ha_iniciat_la_jornada_data_amb_€(String date ,double initialMoney) throws Throwable {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                Date dateOfWorkingDay = sdf.parse(date);
                tryCatch(() -> this.posController.startOfWorkingDay(dateOfWorkingDay, initialMoney));
    }

    @Donat("^que s'ha iniciat el torn el dia \"([^\"]*)\" amb (.+)€$")
        public void que_s_ha_iniciat_la_jornada_data_amb_€(String date ,double initialMoney) throws Throwable {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date dateOfWorkingDay = sdf.parse(date);
        tryCatch(() -> this.posController.startOfWorkingDay(dateOfWorkingDay, initialMoney));
    }

    @Aleshores("^La consulta del dia retorna un desquadrament de (.+)€ el dia \"([^\"]*)\" amb el venedor \"([^\"]*)\" al tpv (\\d+) i sense comentari$")
    public void La_consulta_del_dia_retorna_un_desquadrament_de_€_el_dia_amb_el_venedor_al_tpv_i_sense_comentari(double arg1, String arg3, String arg4, int arg5) throws Throwable {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        WorkingDay wd = posController.getWorkingDay(sdf.parse(arg3));
        assertTrue(wd.getFinished());
        assertEquals(wd.getImbalance(), arg1, 0.001);
        assertTrue(wd.getImbalanceMessage() == null);
        assertTrue(wd.getSaleAssistantName().equals(arg4));
        assertTrue(wd.getPosNumber() == arg5);
    }

    @Donat("^un producte amb nom \"([^\"]*)\", marca \"([^\"]*)\", preu (.+)€, iva (\\d+)% i codi de barres (\\d+)$")
    public void un_producte_amb_nom_marca_preu_€_iva_i_codi_de_barres(String productName,String brandName, double price, int vatPct, int barCode) throws Throwable {
        tryCatch(() -> this.posController.newProductWithBrand(productName, brandName, price, vatPct, barCode));
    }

    @Donat("^que hi ha un descompte percentual del (\\d+)% sobre el producte \"([^\"]*)\"$")
    public void hiHaUnDescomptePercentualDelSobreElProducte(int discountRate, String productName) throws Throwable {
        Product p = posController.findProductByName(productName);
        ArrayList<Product> list = new ArrayList<>(Collections.singleton(p));
        Date now = new Date();
        Date farFuture = new Date(System.currentTimeMillis() + 100000000L);
        tryCatch(() -> posController.newPercentDiscount(list, now, farFuture, discountRate / 100d));
    }

    @I("^la venta actual té (\\d+) descomptes aplicats$")
    public void laVentaActualTindràDescomptesAplicats(int numDiscounts) throws Throwable {
        assertEquals(numDiscounts, posController.getCurrentSale().getAppliedDiscounts().size());
    }

    /**Descompte**/
    private void setTypeDiscount(String typeDiscount, String value, ArrayList<Product> list, Date dateIni, Date dateFi) throws Exception{
        switch (typeDiscount) {
            case "Fix":
                String[] parts = value.split(" ");
                int quantity = Integer.parseInt(parts[0]);
                tryCatch(()->lastDiscount = posController.newFixDiscount(list, dateIni, dateFi, quantity));
                break;
            case "Percentual":
                String[] parts2 = value.split(" ");
                int discountRate = Integer.parseInt(parts2[0]);
                tryCatch(()->lastDiscount = posController.newPercentDiscount(list, dateIni, dateFi, discountRate/100));
                break;
            case "NxM":
                String[] parts3 = value.split("x");
                int n = Integer.parseInt(parts3[0]);
                int m = Integer.parseInt(parts3[1]);
                tryCatch(()->lastDiscount = posController.newNxMDiscount(list, dateIni, dateFi, n, m));
                break;
            case "Regal":
                List<String> productsGifted = Files.readAllLines(Paths.get(getClass().getResource("/" + value).toURI()));
                ArrayList<Product> listGifted = new ArrayList<>();
                for (String s: productsGifted){
                    Product p2 = posController.findProductByName(s);
                    listGifted.add(p2);
                }
                tryCatch(()->lastDiscount = posController.newGiftDiscount(list, dateIni, dateFi, listGifted));
                break;
        }
    }

    @Quan("^vull donar de alta un descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per al producte \"([^\"]*)\" sense data indicada$")
    public void altaDescompte1(String typeDiscount, String value, String productName) throws Exception {
        Product p = posController.findProductByName(productName);
        ArrayList<Product> list = new ArrayList<>(Collections.singleton(p));
        setTypeDiscount(typeDiscount, value, list, null, null);
    }

    @Quan("^vull donar de alta un descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per al producte \"([^\"]*)\" amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\"$")
    public void altaDescompte2(String typeDiscount, String value, String productName, String dataIni, String dataFi) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);
        Product p = posController.findProductByName(productName);
        ArrayList<Product> list = new ArrayList<>(Collections.singleton(p));
        setTypeDiscount(typeDiscount,value,list,dateIni,dateFi);
    }

    @Quan("^vull donar de alta un descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per als productes \"([^\"]*)\" sense data indicada$")
    public void altaDescompte3(String typeDiscount, String value, String productList) throws Exception {
        List<String> products = Files.readAllLines(Paths.get(getClass().getResource("/" + productList).toURI()));
        ArrayList<Product> list = new ArrayList<>();
        for (String s: products){
            Product p = posController.findProductByName(s);
            list.add(p);
        }
        setTypeDiscount(typeDiscount, value, list, null, null);
    }

    @Quan("^vull donar de alta un descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per als productes \"([^\"]*)\" amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\"$")
    public void altaDescompte4(String typeDiscount, String value, String productList, String dataIni, String dataFi) throws Exception {
        List<String> products = Files.readAllLines(Paths.get(getClass().getResource("/"+productList).toURI()));
        ArrayList<Product> list = new ArrayList<>();
        for (String s: products){
            Product p = posController.findProductByName(s);
            list.add(p);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);

        setTypeDiscount(typeDiscount, value, list, dateIni, dateFi);
    }

    @Quan("^vull donar de alta un descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per la marca \"([^\"]*)\" sense data indicada$")
    public void altaDescompte5(String typeDiscount, String value, String brand) throws Exception {
        ArrayList<Product> list = posController.findProductByBrand(brand);
        setTypeDiscount(typeDiscount, value, list, null, null);
    }

    @Quan("^vull donar de alta un descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per la marca \"([^\"]*)\" amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\"$")
    public void altaDescompte6(String typeDiscount, String value, String brand, String dataIni, String dataFi) throws Exception {
        ArrayList<Product> list = posController.findProductByBrand(brand);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);

        setTypeDiscount(typeDiscount, value, list, dateIni, dateFi);
    }

    @Quan("^vull donar de alta un descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per a tota la venta sense data indicada$")
    public void altaDescompte7(String typeDiscount, String value) throws Exception{
        ArrayList<Product> list = new ArrayList<>();
        setTypeDiscount(typeDiscount, value, list, null, null);
    }

    @Quan("^vull donar de alta un descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per a tota la venta amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\"$")
    public void altaDescompte8(String typeDiscount, String value, String dataIni, String dataFi) throws Exception {
        ArrayList<Product> list = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);
        setTypeDiscount(typeDiscount, value, list, dateIni, dateFi);

    }

    @Quan("^vull donar de alta un val de descompte de (\\d+)€ amb pagament minim de (\\d+)€ sense data indicada i amb codi de barres (\\d+)$")
    public void altaDescompte9(int quantity, int minimumPay, int barCode) {
        tryCatch(() -> lastDiscount = posController.newVoucher(null, null, quantity, minimumPay, barCode));
    }

    @Quan("^vull donar de alta un val de descompte de (\\d+)€ amb pagament minim de (\\d+)€ amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\" i amb codi de barres (\\d+)$")
    public void altaDescompte10(int quantity, int minimumPay, String dataIni, String dataFi, int barCode) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);
        tryCatch(() -> lastDiscount = posController.newVoucher(dateIni, dateFi, quantity, minimumPay,barCode));
    }

    public void checkTypeDiscount(String typeDiscount, String value, Discount d, ArrayList<Product> productsList) throws Exception {
        switch (typeDiscount) {
            case "Fix":
                String[] parts = value.split(" ");
                int quantity = Integer.parseInt(parts[0]);
                FixDiscount dFix = (FixDiscount) d;
                assertTrue(dFix.getDiscountQuantity() == quantity);
                break;
            case "Percentual":
                String[] parts2 = value.split(" ");
                int discountRate = Integer.parseInt(parts2[0]);
                PercentDiscount dPercent = (PercentDiscount) d;
                assertTrue(dPercent.getDiscountRate() == discountRate / 100);
                break;
            case "NxM":
                String[] parts3 = value.split("x");
                int n = Integer.parseInt(parts3[0]);
                int m = Integer.parseInt(parts3[1]);
                NxMDiscount dNxM = (NxMDiscount) d;
                int[] nm = dNxM.getNM();
                assertTrue(nm[0] == n);
                assertTrue(nm[1] == m);
                break;
            case "Regal":
                GiftDiscount dGift = (GiftDiscount) d;
                List<String> productsGifted = Files.readAllLines(Paths.get(getClass().getResource("/" + value).toURI()));
                ArrayList<Product> listGifted = new ArrayList<>();
                for (String s: productsGifted){
                    Product p2 = posController.findProductByName(s);
                    listGifted.add(p2);
                }
                for (Product p2: listGifted) {
                    assertTrue(dGift.isGift(productsList,p2));
                }
                break;
        }
    }


    @Aleshores("s'ha donat de alta el descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per al producte \"([^\"]*)\" sense data indicada$")
     public void sha_donat_de_alta_el_descompte1(String typeDiscount, String value, String productName) throws Exception {
        Discount d = posController.getDiscount(lastDiscount);
        Product p = posController.findProductByName(productName);
        ArrayList<Product> productsList = new ArrayList<>();
        productsList.add(p);
        assertTrue(productsList.size() == 1);
        assertTrue(d != null);
        assertTrue(d.isProductInDiscount(p.getId()));

        assertTrue(d.getDiscountType().equals(typeDiscount));
        checkTypeDiscount(typeDiscount, value, d, productsList);

        assertTrue(d.getInitDate() == null);
        assertTrue(d.getFinalDate() == null);
    }

    @Aleshores("s'ha donat de alta el descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per al producte \"([^\"]*)\" amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\"$")
    public void sha_donat_de_alta_el_descompte2(String typeDiscount, String value, String productName, String dataIni, String dataFi) throws Exception {
        Discount d = posController.getDiscount(lastDiscount);
        Product p = posController.findProductByName(productName);
        ArrayList<Product> productsList = new ArrayList<>();
        productsList.add(p);
        assertTrue(productsList.size() == 1);

        assertTrue(d != null);
        assertTrue(d.isProductInDiscount(p.getId()));

        assertTrue(d.getDiscountType().equals(typeDiscount));
        checkTypeDiscount(typeDiscount, value, d, productsList);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);
        assertTrue(d.getInitDate().equals(dateIni));
        assertTrue(d.getFinalDate().equals(dateFi));
    }

    @Aleshores("s'ha donat de alta el descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per als productes \"([^\"]*)\" sense data indicada$")
    public void sha_donat_de_alta_el_descompte3(String typeDiscount, String value, String productList) throws Exception  {
        Discount d = posController.getDiscount(lastDiscount);
        ArrayList<Product> productsList = new ArrayList<>();
        for (String s: Files.readAllLines(Paths.get(getClass().getResource("/"+productList).toURI()))) {
            productsList.add(posController.findProductByName(s));
        }

        assertTrue(d != null);

        for (Product p: productsList) {
            assertTrue(d.isProductInDiscount(p.getId()));
        }

        assertTrue(d.getDiscountType().equals(typeDiscount));
        checkTypeDiscount(typeDiscount,value,d,productsList);

        assertTrue(d.getInitDate() == null);
        assertTrue(d.getFinalDate() == null);
    }

    @Aleshores("s'ha donat de alta el descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per als productes \"([^\"]*)\" amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\"$")
    public void sha_donat_de_alta_el_descompte4(String typeDiscount, String value, String productList, String dataIni, String dataFi) throws Exception  {
        Discount d = posController.getDiscount(lastDiscount);
        ArrayList<Product> productsList = new ArrayList<>();
        for (String s: Files.readAllLines(Paths.get(getClass().getResource("/"+productList).toURI()))) {
            productsList.add(posController.findProductByName(s));
        }

        assertTrue(d != null);

        for (Product p: productsList) {
            assertTrue(d.isProductInDiscount(p.getId()));
        }

        assertTrue(d.getDiscountType().equals(typeDiscount));
        checkTypeDiscount(typeDiscount, value, d, productsList);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);
        assertTrue(d.getInitDate().equals(dateIni));
        assertTrue(d.getFinalDate().equals(dateFi));
    }

    @Aleshores("s'ha donat de alta el descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per la marca \"([^\"]*)\" sense data indicada$")
    public void sha_donat_de_alta_el_descompte5(String typeDiscount, String value, String brand) throws Exception{
        Discount d = posController.getDiscount(lastDiscount);
        ArrayList<Product> productsList = posController.findProductByBrand(brand);

        assertTrue(d != null);

        for (Product p: productsList) {
            assertTrue(d.isProductInDiscount(p.getId()));
        }

        assertTrue(d.getDiscountType().equals(typeDiscount));
        checkTypeDiscount(typeDiscount,value,d,productsList);

        assertTrue(d.getInitDate() == null);
        assertTrue(d.getFinalDate() == null);

    }

    @Aleshores("s'ha donat de alta el descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per la marca \"([^\"]*)\" amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\"$")
    public void sha_donat_de_alta_el_descompte6(String typeDiscount, String value, String brand, String dataIni, String dataFi) throws Exception {
        Discount d = posController.getDiscount(lastDiscount);
        ArrayList<Product> productsList = posController.findProductByBrand(brand);

        assertTrue(d != null);

        for (Product p: productsList) {
            assertTrue(d.isProductInDiscount(p.getId()));
        }

        assertTrue(d.getDiscountType().equals(typeDiscount));
        checkTypeDiscount(typeDiscount, value, d, productsList);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);
        assertTrue(d.getInitDate().equals(dateIni));
        assertTrue(d.getFinalDate().equals(dateFi));
    }

    @Aleshores("s'ha donat de alta el descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per a tota la venta sense data indicada$")
    public void sha_donat_de_alta_el_descompte7(String typeDiscount, String value) throws Exception {
        Discount d = posController.getDiscount(lastDiscount);

        assertTrue(d != null);

        assertTrue(d.getProductList().isEmpty());

        assertTrue(d.getDiscountType().equals(typeDiscount));
        checkTypeDiscount(typeDiscount,value,d,new ArrayList<>());

        assertTrue(d.getInitDate() == null);
        assertTrue(d.getFinalDate() == null);
    }

    @Aleshores("s'ha donat de alta el descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" per a tota la venta amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\"$")
    public void sha_donat_de_alta_el_descompte8(String typeDiscount, String value, String dataIni, String dataFi) throws Exception{
        Discount d = posController.getDiscount(lastDiscount);

        assertTrue(d != null);

        assertTrue(d.getProductList().isEmpty());

        assertTrue(d.getDiscountType().equals(typeDiscount));
        checkTypeDiscount(typeDiscount, value, d, new ArrayList<>());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);
        assertTrue(d.getInitDate().equals(dateIni));
        assertTrue(d.getFinalDate().equals(dateFi));
    }

    @Aleshores("s'ha donat de alta un val de descompte de (\\d+)€ amb pagament minim de (\\d+)€ sense data indicada i amb codi de barres (\\d+)$")
    public void sha_donat_de_alta_el_descompte9(int quantity,  int minimumPay, int barCode) throws Exception{
        Discount d = posController.getDiscount(lastDiscount);
        assertTrue(d != null);

        assertTrue(d.getProductList().isEmpty());

        Voucher dV = (Voucher) d;
        assertTrue(dV.getDiscountQuantity() == quantity);
        assertTrue(dV.getMinimumPay() == minimumPay);
        assertTrue(dV.getBarCode()==barCode);

        assertTrue(d.getInitDate() == null);
        assertTrue(d.getFinalDate() == null);
    }

    @Aleshores("s'ha donat de alta un val de descompte de (\\d+)€ amb pagament minim de (\\d+)€ amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\" i amb codi de barres (\\d+)$")
    public void sha_donat_de_alta_el_descompte9(int quantity, int minimumPay, String dataIni, String dataFi, int barCode) throws Exception{
        Discount d = posController.getDiscount(lastDiscount);

        assertTrue(d != null);

        assertTrue(d.getProductList().isEmpty());

        Voucher dV = (Voucher) d;
        assertTrue(dV.getDiscountQuantity() == quantity);
        assertTrue(dV.getMinimumPay() == minimumPay);
        assertTrue(dV.getBarCode()==barCode);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);
        assertTrue(d.getInitDate().equals(dateIni));
        assertTrue(d.getFinalDate().equals(dateFi));
    }

    @Quan("^el client vol pagar amb \"([^\"]*)\" i que el client aporta (.+)€ i vull calcular el canvi a tornar$")
    public void el_client_vol_pagar_amb_i_que_el_client_aporta_€_i_vull_calcular_el_canvi_a_tornar(String arg1, double arg2) throws Throwable {
        tryCatch(() -> this.change = this.posController.payment(arg1, arg2));
        if (exception==null) {
            tryCatch(() -> ticket = this.posController.printTicket());
        }
    }

    @Quan("^el client vol pagar amb \"([^\"]*)\" i vull calcular el canvi a tornar$")
    public void el_client_vol_pagar_amb_i_vull_calcular_el_canvi_a_tornar(String arg1) throws Throwable {
        tryCatch(() -> this.change = this.posController.payment(arg1, 0.0));
        tryCatch(() -> ticket = this.posController.printTicket());
    }

    @I("^que hi ha un descompte fixe de (\\d+)€ sobre la venta$")
    public void que_hi_ha_un_descompte_fixe_de_€_sobre_la_venta(int arg1) throws Throwable {
        tryCatch(() -> posController.newFixDiscount(new ArrayList<>(), null, null, arg1));
    }

    @I("^que hi ha un altre descompte fixe de (\\d+)€ sobre la venta$")
    public void que_hi_ha_un_altre_descompte_fixe_de_€_sobre_la_venta(int arg1) throws Throwable {
        tryCatch(() -> posController.newFixDiscount(new ArrayList<>(), null, null, arg1));
    }

    @Donat("^que hi ha un (\\d+)x(\\d+) en \"([^\"]*)\"$")
    public void que_hi_ha_un_x_en(int arg1, int arg2, String arg3) throws Throwable {
        tryCatch(() -> posController.newNxMDiscount(null, null, arg3, posController.getProductsService(), arg1, arg2));
    }

    @Donat("^que hi ha un (\\d+)x(\\d+) en el producte \"([^\"]*)\"$")
    public void queHiHaUnXEnElProducte(int n, int m, String productName) throws Throwable {
        ArrayList<Product> products = new ArrayList<>();
        products.add(posController.findProductByName(productName));
        tryCatch(() -> posController.newNxMDiscount(products, null, null, n, m));
    }

    @Donat("^que per la compra d'un producte \"([^\"]*)\" es regala una unitat del producte \"([^\"]*)\"$")
    public void que_per_la_compra_d_un_producte_es_regala_una_unitat_del_producte(String arg1, String arg2) throws Throwable {
        ArrayList<Product> as = new ArrayList<>();
        as.add(posController.findProductByName(arg1));
        ArrayList<Product> us = new ArrayList<>();
        us.add(posController.findProductByName(arg2));
        tryCatch(() -> posController.newGiftDiscount(as, null, null, us));
    }

    @Donat("^que hi ha un usuari administrador \"([^\"]*)\" amb contrasenya \"([^\"]*)\"$")
    public void que_hi_ha_un_usuari_administrador_amb_contrassenya(String username, String pass) throws Throwable {
        jefe = username;
        contraJefe = pass;
        tryCatch(() -> posController.firstAdministratorUser(username, pass));
    }

    @Donat("^que hi ha un treballador amb nom d'usuari \"([^\"]*)\" i contrasenya \"([^\"]*)\"$")
    public void que_hi_ha_un_treballador_amb_nom_d_usuari_i_contrassenya(String name, String pass) throws Throwable {
        posController.login(jefe, contraJefe);
        tryCatch(() -> posController.newWorkerUser(name, pass));
        posController.logout();
    }

    @Quan("^vull donar d'alta un nou treballador amb nom \"([^\"]*)\" i contrasenya \"([^\"]*)\"$")
    public void vull_donar_d_altra_un_nou_treballador_amb_nom_i_contrassenya(String nom, String pass) throws Throwable {
        tryCatch(() -> posController.newWorkerUser(nom, pass));
    }

    @Aleshores ("^S'ha donat d'alta un nou treballador amb nom \"([^\"]*)\" i contrasenya \"([^\"]*)\"$")
    public void sha_donat_dalta_un_nou_treballador_amb_nom_i_contrasenya(String name, String pass) throws Throwable {
        User u = posController.getWorkerUser(name);
        assertEquals(u.getClass().getSimpleName(), "Worker");
        assertEquals(u.getUsername(), name);
        assertEquals(u.getPassword(), pass);
    }

    @Donat("^que el cap ha introduit un descompte percentual del (\\d+)% sobre el producte \"([^\"]*)\"$")
    public void que_el_cap_ha_introduit_un_descompte_percentual_del_sobre_el_producte(int discountRate, String productName) throws Throwable {
        posController.login(jefe,contraJefe);
        Product p = posController.findProductByName(productName);
        ArrayList<Product> list = new ArrayList<>(Collections.singleton(p));
        Date now = new Date();
        Date farFuture = new Date(System.currentTimeMillis() + 100000000L);
        tryCatch(() -> posController.newPercentDiscount(list, now, farFuture, discountRate / 100d));
        posController.logout();
    }

    @Donat("^que el cap ha introduit un descompte (\\d+)x(\\d+) en \"([^\"]*)\"$")
    public void queElCapHaIntroduitUnDescompteXEn(int n, int m, String brand) throws Throwable {
        posController.login(jefe,contraJefe);
        Date now = new Date();
        Date farFuture = new Date(System.currentTimeMillis() + 100000000L);
        tryCatch(() -> posController.newNxMDiscount(now, farFuture, brand, posController.getProductsService(), n, m));
        posController.logout();
    }

    @Quan("^vull donar d'alta un nou cap de botiga amb nom \"([^\"]*)\" i contrasenya \"([^\"]*)\"$")
    public void vull_donar_d_alta_un_nou_cap_de_botiga_amb_nom_i_contrasenya(String nom, String pass) throws Throwable {
        tryCatch(() -> posController.newStoreManegerUser(nom, pass));
    }

    @Aleshores("^S'ha donat d'alta un nou cap de botiga  amb nom \"([^\"]*)\" i contrasenya \"([^\"]*)\"$")
    public void S_ha_donat_d_alta_un_nou_cap_de_botiga_amb_nom_i_contrasenya(String name, String pass) throws Throwable {
        User u = posController.getWorkerUser(name);
        assertEquals(u.getClass().getSimpleName(),"StoreManager");
        assertEquals(u.getUsername(), name);
        assertEquals(u.getPassword(), pass);
    }

    @Quan("^vull donar d'alta un nou administrador amb nom \"([^\"]*)\" i contrasenya \"([^\"]*)\"$")
    public void vull_donar_d_alta_un_nou_administrador_amb_nom_i_contrasenya(String nom, String pass) throws Throwable {
        tryCatch(() -> posController.newAdministratorUser(nom, pass));
    }

    @Aleshores("^S'ha donat d'alta un nou administrador amb nom \"([^\"]*)\" i contrasenya \"([^\"]*)\"$")
    public void S_ha_donat_d_alta_un_nou_administrador_amb_nom_i_contrasenya(String name, String pass) throws Throwable {
        User u = posController.getWorkerUser(name);
        assertEquals(u.getClass().getSimpleName(), "Administrator");
        assertEquals(u.getUsername(), name);
        assertEquals(u.getPassword(), pass);
    }

    @I("^que hi ha un cap de botiga \"([^\"]*)\" amb contrassenya \"([^\"]*)\"$")
    public void que_hi_ha_un_cap_de_botiga_amb_contrassenya(String name, String pass) throws Throwable {
        posController.login(jefe, contraJefe);
        tryCatch(() -> posController.newStoreManegerUser(name, pass));
        posController.logout();
    }

    @Quan("^trec el producte amb codi de barres (\\d+) a la venta$")
    public void trec_el_producte_amb_codi_de_barres_a_la_venta(int barCode) throws Throwable {
        tryCatch(() -> this.posController.removeProductByBarCode(barCode));
    }

    @Donat ("^que el cap ha introduit un val de descompte amb (\\d+)€ de descompte per a compres superiors a (\\d+)€ sense dates amb codi de barres (\\d+)$")
    public void queElCapHaIntroduitUnValDeDescompte(int discountQuantity, int minimumPay, int barCode) throws Throwable {
        posController.newVoucher(null, null, discountQuantity, minimumPay, barCode);
    }

    @Quan ("^el client entrega un val de descompte amb codi de barres (\\d+)$")
    public void elClientEntregaUnValDeDescompte(int barCode) throws Throwable {
        tryCatch(() -> posController.applyVoucher(barCode));
    }

    @Donat ("^que el cap ha introduit un val de descompte amb (\\d+)€ de descompte per a compres superiors a (\\d+)€ amb data inici \"([^\"]*)\" i data fi \"([^\"]*)\" amb codi de barres (\\d+)$")
    public void queElCapHaIntroduitUnValDeDescompte(int discountQuantity, int minimumPay, String dataIni, String dataFi, int barCode) throws Throwable {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = dateFormat.parse(dataIni);
        Date dateFi = dateFormat.parse(dataFi);
        tryCatch(() -> posController.newVoucher(dateIni, dateFi, discountQuantity, minimumPay, barCode));
    }

    @Quan("^vull afegir un producte amb nom \"([^\"]*)\", preu (.+)€, iva (\\d+)% i codi de barres (\\d+)$")
    public void vull_afegir_un_producte_amb_nom_preu_€_iva_i_codi_de_barres(String name, double price, int iva, int barCode) throws Throwable {
        tryCatch(() -> posController.addNewProduct(name, price, iva, barCode));
    }

    @Aleshores("^hi ha un producte amb nom \"([^\"]*)\", preu (.+)€, iva (\\d+)% i codi de barres (\\d+)$")
    public void hi_ha_un_producte_amb_nom_preu_€_iva_i_codi_de_barres(String name, double price, double iva, int barCode) throws Throwable {
        Product p = posController.getProductsService().findByBarCode(barCode);
        assertNotNull(p);
        assertEquals(name, p.getName());
        assertTrue(price == p.getPrice());
        assertTrue(iva == p.getVatPct());
    }

    @Quan("^vull afegir un producte amb nom \"([^\"]*)\", marca \"([^\"]*)\" preu (.+)€, iva (\\d+)% i codi de barres (\\d+)$")
    public void vull_afegir_un_producte_amb_nom_marca_preu_€_iva_i_codi_de_barres(String name, String brand, double price, int iva, int barCode) throws Throwable {
        tryCatch(() -> posController.newProductWithBrand(name, brand, price, iva, barCode));
    }

    @Aleshores("^hi ha un producte amb nom \"([^\"]*)\", marca \"([^\"]*)\" preu (.+)€, iva (\\d+)% i codi de barres (\\d+)$")
    public void hi_ha_un_producte_amb_nom_marca_preu_€_iva_i_codi_de_barres(String name, String brand, double price, int iva, int barCode) throws Throwable {
        Product p = posController.getProductsService().findByBarCode(barCode);
        assertNotNull(p);
        assertEquals(name, p.getName());
        assertEquals(brand, p.getBrand());
        assertTrue(price == p.getPrice());
        assertTrue(iva == p.getVatPct());
    }

    @I("^un descompte del tipus \"([^\"]*)\" de \"([^\"]*)\" amb el numero (\\d+) per la marca \"([^\"]*)\" sense data indicada$")
    public void un_descompte_del_tipus_de_amb_el_numero_per_la_marca_sense_data_indicada(String typeDiscount, String value, int idDiscount, String brand) throws Throwable {
        switch (typeDiscount) {
            case "Fix":
                String[] parts = value.split(" ");
                int quantity = Integer.parseInt(parts[0]);
                posController.newFixDiscount(null, null, brand, posController.getProductsService(), quantity);
                break;
            case "Percentual":
                String[] parts2 = value.split(" ");
                int discountRate = Integer.parseInt(parts2[0]);
                posController.newPercentDiscount(null, null, brand, posController.getProductsService(), discountRate/100);
                break;
            case "NxM":
                String[] parts3 = value.split("x");
                int n = Integer.parseInt(parts3[0]);
                int m = Integer.parseInt(parts3[1]);
                posController.newNxMDiscount( null, null, brand, posController.getProductsService(), n, m);
                break;
            case "Regal":
                List<String> productsGifted = Files.readAllLines(Paths.get(getClass().getResource("/" + value).toURI()));
                ArrayList<Product> listGifted = new ArrayList<>();
                for (String s: productsGifted){
                    Product p2 = posController.findProductByName(s);
                    listGifted.add(p2);
                }
                posController.newGiftDiscount( null,  null, brand, posController.getProductsService(), listGifted);
                break;
        }
    }

    @Aleshores("^el descompte (\\d+) fara descompte al producte amb codi de barres (\\d+)$")
    public void el_descompte_fara_descompte_al_producte_amb_codi_de_barres(int idDiscount, int productBarCode) throws Throwable {
        Discount d = posController.getDiscount(idDiscount);
        assertTrue(d.isProductInDiscount(posController.getProductsService().findByBarCode(productBarCode).getId()));
    }

    @Quan("^vull afegir un producte amb codi de barres (\\d+)$")
    public void vull_afegir_un_producte_amb_codi_de_barres(int barCode) throws Throwable {
        tryCatch(() -> posController.addNewProduct(barCode));
    }

    @Aleshores("^hi ha un producte amb codi de barres (\\d+)$")
    public void hi_ha_un_producte_amb_codi_de_barres(int barCode) throws Throwable {
        Product p = posController.getProductsService().findByBarCode(barCode);
        assertNotNull(p);
    }

    @Donat("^que vull afegir un producte amb codi de barres (\\d+)$")
    public void que_vull_afegir_un_producte_amb_codi_de_barres(int barCode) throws Throwable {
        vull_afegir_un_producte_amb_codi_de_barres(barCode);
    }

    @Quan("^al producte amb codi de barres (\\d+) vull posar-l'hi el preu (.+)€$")
    public void al_producte_amb_codi_de_barres_vull_posar_l_hi_el_preu_€(int barCode, double price) throws Throwable {
        tryCatch(() -> posController.setPriceToProduct(price, barCode));
    }

    @Quan("^al producte amb codi de barres (\\d+) vull posar-l'hi iva del (\\d+)%$")
    public void al_producte_amb_codi_de_barres_vull_posar_l_hi_iva_del_(int barCode, int iva) throws Throwable {
        tryCatch(() -> posController.setVatPct(barCode, iva));
    }

    @Quan("^al producte amb codi de barres (\\d+) vull posar-l'hi el nom \"([^\"]*)\"$")
    public void al_producte_amb_codi_de_barres_vull_posar_l_hi_el_nom(int barCode, String name) throws Throwable {
        tryCatch(() ->posController.setProductName(barCode, name));
    }

    @Quan("^faig el recompte indican't que hi han (.+)€ a caixa$")
    public void fare_el_recompte_i_indicare_que_hi_han_€_a_caixa(double money) throws Throwable {
        fare_recompte_i_veure_que_hi_han_€_a_caixa(money);
    }

    @Quan("^tanqui la jornada amb els (.+)€ i deixant el missatge \"([^\"]*)\"$")
    public void tanqui_la_jornada_amb_els_€_i_deixant_el_missatge(double money, String msg) throws Throwable {
        this.posController.closeWorkingDay(money, msg);
    }

    @Donat("^va fer logout$")
    public void va_fer_logout() throws Throwable {
        tryCatch(() -> this.posController.logout());
    }

    @Aleshores("^es genera un informe com l'\"([^\"]*)\"$")
    public void esGeneraUnInformeComL(String nomInforme) throws Throwable {
        byte[] report1s = Files.readAllBytes(Paths.get(getClass().getResource("/informes/"+nomInforme).toURI()));
        String report = new String(report1s, "UTF-8");
        assertEquals(report, this.report);
    }

    @Quan("^vull generar un informe de totes les vendes$")
    public void vullGenerarUnInformeDeTotesLesVendes() throws Throwable {
        report = posController.getAllSalesReport();
    }

    @Donat("^que es va fer la venda (\\d+) el dia \"([^\"]*)\" a les \"([^\"]*)\" atesa per \"([^\"]*)\" i pagada amb mètode \"([^\"]*)\" amb (\\d+) euros de diners donats i amb productes:$")
    public void queEsVaFerLaVendaElDiaALesAtesaPerIPagadaAmbMètodeAmbProductes(int saleId, String dayStr, String hourStr, String saleAss, String method, int given, List<String> productNames) throws Throwable {
        String shop = posController.getShopName();
        String address = posController.getAddress();
        int pos = posController.getPosNumber();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = dateFormat.parse(dayStr + " " + hourStr);
        Sale sale = new Sale(shop, address, pos, saleAss, date);
        sale.setSaleAssistantName(saleAss);
        sale.setId(saleId);
        for (String name : productNames) {
            sale.addProduct(posController.getProductsService().findByName(name));
        }
        posController.startOfWorkingDay(date, 50);
        posController.setCurrentSale(sale);
        posController.bestDiscountsForSale();
        sale.setPaymentMethod(method);
        if (sale.getPaymentMethod().equals("Efectiu")) sale.setGivenMoney(given);
        posController.closeSale();
        posController.closeWorkingDay(100, ";)");
    }

    @Donat("^un client fidelitzat amb nom \"([^\"]*)\" i numero de telefon \"([^\"]*)\"$")
    public void clientFidelitzatCreated(String name, String phoneNumber) throws Throwable {
        tryCatch(() -> posController.newLoyalCostumer(phoneNumber, name));
    }

    @Donat("^un client fidelitzat amb nom \"([^\"]*)\", numero de telefon \"([^\"]*)\" i (\\d+) punts$")
    public void clientFidelitzatCreated2(String name, String phoneNumber, int points) throws Throwable {
        tryCatch(() -> posController.newLoyalCostumer(phoneNumber, name, points));
    }

    @Quan("^vull afegir un client fidelitzat amb nom \"([^\"]*)\" i numero de telefon \"([^\"]*)\"$")
    public void vull_afegir_un_client_fidelitzat(String name, String phoneNumber) throws Throwable {
        tryCatch(() -> posController.newLoyalCostumer(phoneNumber, name));
    }

    @Aleshores("^hi ha un client fidelitzat amb nom \"([^\"]*)\", numero de telefon \"([^\"]*)\" i amb (\\d+) punts$")
    public void hi_ha_un_client_fidelitzat(String name, String phoneNumber, int points) throws Throwable {
        LoyalCostumer lc = posController.getLoyalCostumerService().findByPhoneNumber(phoneNumber);
        assertNotNull(lc);
        assertEquals(name, lc.getName());
        assertEquals(phoneNumber, lc.getPhoneNumber());
        assertTrue(points == lc.getPoints());
    }

    @I("^es guarda el missatge \"([^\"]*)\"$")
    public void es_guarda_el_missatge(String msg) throws Throwable {
        WorkingDay wd = this.posController.getWorkingDay(date);
        assertEquals(msg, wd.getImbalanceMessage());
    }

    @I("^amb descuadrament (.+)€$")
    public void amb_descuadrament_€(double value) throws Throwable {
        WorkingDay wd = this.posController.getWorkingDay(date);
        assertTrue(wd.getImbalance() == value);
    }

    @Donat("^el client s'identifica com a fidelitzat donant el numero de telefon \"([^\"]*)\"$")
    public void el_client_sidentifica(String phoneNumber) {
        tryCatch(() -> posController.setLoyalCostumer(phoneNumber));
        //tryCatch(() -> posController.setLoyalCostumerPoints(phoneNumber));
    }

    @Aleshores("^el client fidelitzat amb numero de telefon \"([^\"]*)\" té (\\d+) punts$")
    public void el_client_fidelitzat_te(String phoneNumber, int points) {
        LoyalCostumer lc = posController.getLoyalCostumerService().findByPhoneNumber(phoneNumber);
        assertTrue(points == lc.getPoints());
    }
}