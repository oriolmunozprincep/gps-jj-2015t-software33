# language: ca

#noinspection SpellCheckingInspection
Característica: Aplicar Descomptes a un Producte d'una Venda

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Oriol Pellisa" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Playmobil", preu 5,00€, iva 0% i codi de barres 123
    I un producte amb nom "Robocop", preu 6,00€, iva 0% i codi de barres 456
    I un producte amb nom "Zafiro", preu 23,95€, iva 0% i codi de barres 789
    I va fer logout
    I que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 1000€
    I que hi ha una venta iniciada amb data "25/11/2015" i hora "10:00"

  Escenari: Aplicar únic descompte a únic producte
    Donat que hi ha un descompte percentual del 10% sobre el producte "Playmobil"
    Quan afegeixo el producte de codi de barres 123 a la venta
    Aleshores el total de la venta actual és de 4,50€
    I la venta actual té 1 descomptes aplicats

  Escenari: Si hi ha dos en el mateix producte, s'aplica el que implica preu més baix
    Donat que hi ha un descompte percentual del 10% sobre el producte "Playmobil"
    I que hi ha un descompte percentual del 50% sobre el producte "Playmobil"
    Quan afegeixo el producte de codi de barres 123 a la venta
    Aleshores el total de la venta actual és de 2,50€
    I la venta actual té 1 descomptes aplicats

  Escenari: El descompte s'aplica sobre totes les unitats
    Donat que hi ha un descompte percentual del 50% sobre el producte "Playmobil"
    I afegeixo el producte de codi de barres 123 a la venta
    Quan afegeixo el producte de codi de barres 123 a la venta
    Aleshores el total de la venta actual és de 5,00€
    I la venta actual té 1 descomptes aplicats

  Escenari: Dos descomptes en dos productes
    Donat que hi ha un descompte percentual del 10% sobre el producte "Playmobil"
    I que hi ha un descompte percentual del 10% sobre el producte "Robocop"
    I afegeixo el producte de codi de barres 123 a la venta
    Quan afegeixo el producte de codi de barres 456 a la venta
    Aleshores el total de la venta actual és de 9,90€
    I la venta actual té 2 descomptes aplicats