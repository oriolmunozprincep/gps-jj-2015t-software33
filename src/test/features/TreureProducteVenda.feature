# language: ca

#noinspection SpellCheckingInspection
Característica: Treure producte a una venda

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
    I un producte amb nom "Second", preu 20€, iva 15% i codi de barres 98765
    I un producte amb nom "Third", preu 0,5€, iva 20% i codi de barres 654321
    I va fer logout
    I que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 500€
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 1234567 a la venta
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 98765 a la venta

  Escenari: Treure un producte que te mes d'una unitat
    Quan trec el producte amb codi de barres 654321 a la venta
    Aleshores la venta té 3 línies
    I línia de venta 1 és de 1 unitats de "Third" a 0,6€ cada una per un total de 0,6€
    I línia de venta 2 és de 1 unitats de "Optimus Prime" a 27,83€ cada una per un total de 27,83€
    I línia de venta 3 és de 1 unitats de "Second" a 23€ cada una per un total de 23€
    I la pantalla del client del tpv mostra
    """
    Third - 0.6€/u x 1u = 0.6€
    Optimus Prime - 27.83€/u x 1u = 27.83€
    Second - 23.0€/u x 1u = 23.0€
    ---
    Total: 51.43€
    """

  Escenari: Treure un producte que nomes quedi una unitat
    Quan trec el producte amb codi de barres 1234567 a la venta
    Aleshores la venta té 2 línies

    Escenari: Tancar venda quan no quedin mes preductes
      Donat afegeixo el producte de codi de barres 1234567 a la venta
      I trec el producte amb codi de barres 1234567 a la venta
      Quan tanco la venta
      Aleshores no sortira cap missatge d'error