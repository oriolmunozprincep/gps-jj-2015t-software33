# language: ca

#noinspection SpellCheckingInspection
Característica: Generar el tiquet

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Oriol Pellisa" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Playmobil", preu 5,00€, iva 0% i codi de barres 123
    I un producte amb nom "Robocop", preu 6,00€, iva 0% i codi de barres 456
    I un producte amb nom "Zafiro", preu 23,95€, iva 0% i codi de barres 789
    I un producte amb nom "Croissant", preu 1€, iva 10% i codi de barres 111
    I un producte amb nom "JoguIVA", preu 10€, iva 21% i codi de barres 222
    I va fer logout
    I que en "Oriol Pellisa" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 1000€
    I que hi ha una venta iniciada amb data "25/11/2015" i hora "10:00"

  Escenari: Generar el tiquet amb 1 producte afegit
    Donat afegeixo el producte de codi de barres 123 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticket1"


  Escenari: Generar el tiquet amb 1 producte afegit dos cops
    Donat afegeixo el producte de codi de barres 123 a la venta
    Donat afegeixo el producte de codi de barres 123 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticket2"


  Escenari: Generar el tiquet amb 2 productes afegits
    Donat afegeixo el producte de codi de barres 123 a la venta
    Donat afegeixo el producte de codi de barres 456 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticket3"

  Escenari: Generar el tiquet amb 3 productes afegits
    Donat afegeixo el producte de codi de barres 123 a la venta
    Donat afegeixo el producte de codi de barres 456 a la venta
    Donat afegeixo el producte de codi de barres 789 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticket4"

  Escenari: Generar el tiquet amb 3 desglossaments diferents
    Donat afegeixo el producte de codi de barres 123 a la venta
    Donat afegeixo el producte de codi de barres 123 a la venta
    Donat afegeixo el producte de codi de barres 111 a la venta
    Donat afegeixo el producte de codi de barres 222 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticket3desglossaments"
