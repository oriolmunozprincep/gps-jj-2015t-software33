# language: ca

#noinspection SpellCheckingInspection
Característica: Configurar Tpv

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Oriol Pellisa" i contrasenya "123"
    I que en "Oriol Pellisa" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 500€


  Escenari: Canviar el nom de la botiga
    Donat que vull canviar el nom de la botiga a "Tois ar as"
    Quan vull generar un tiquet
    Aleshores el nom que sortira al tiquet sera "Tois ar as"

  Escenari: Canviar el numero de TPV
    Donat que vull canviar el numero del TPV al 8
    Quan  vull generar un tiquet
    Aleshores el numero de TPV que sortira al tiquet sera el 8