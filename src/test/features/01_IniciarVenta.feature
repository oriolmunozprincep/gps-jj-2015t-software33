# language: ca

#noinspection SpellCheckingInspection
Característica: Iniciar una venta

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "123"
    I que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 500€

  Escenari: Iniciar una venta
    Quan inicio una nova venta
    Aleshores la venta actual és de'n "Joan" al tpv 1 de la botiga "Girona 1"
    I la pantalla del client del tpv mostra
    """
    Li donem la benvinguda a Joguets i Joguines!
    L'atén Joan
    """

  Escenari: No es pot iniciar una venta si ja hi ha una venta iniciada
    Donat inicio una nova venta
    Quan inicio una nova venta
    Aleshores obtinc un error que diu: "Aquest tpv ja té una venta iniciada"