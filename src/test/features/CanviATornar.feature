# language: ca

#noinspection SpellCheckingInspection
Característica: Canvi a tornar

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Oriol Pellisa" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Playmobil", preu 5,00€, iva 0% i codi de barres 123
    I un producte amb nom "Robocop", preu 6,00€, iva 0% i codi de barres 456
    I un producte amb nom "Zafiro", preu 23,95€, iva 0% i codi de barres 789
    I va fer logout
    I que en "Oriol Pellisa" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 1000€
    I que hi ha una venta iniciada amb data "25/11/2015" i hora "10:00"

  Escenari: Calcular canvi a tornar quan el client paga amb efectiu suficient
    Donat afegeixo el producte de codi de barres 123 a la venta
    Quan el client vol pagar amb "Efectiu" i que el client aporta 8,00€ i vull calcular el canvi a tornar
    Aleshores el canvi a tornar de la venta ha sigut 3,00€
    I es genera un tiquet amb tota la informació de la venta igual a "ticketWithChange1"

  Escenari: Calcular canvi a tornar quan el client paga amb efectiu insuficient
    Donat afegeixo el producte de codi de barres 123 a la venta
    Quan el client vol pagar amb "Efectiu" i que el client aporta 3,0€ i vull calcular el canvi a tornar
    Aleshores el canvi a tornar de la venta ha sigut -2,00€
    I es genera un tiquet amb tota la informació de la venta igual a "ticketWithChange2"

  Escenari: Calcular canvi a tornar quan el client paga amb targeta
    Donat afegeixo el producte de codi de barres 123 a la venta
    Quan el client vol pagar amb "Targeta" i vull calcular el canvi a tornar
    Aleshores el canvi a tornar de la venta ha sigut 0,00€
    I es genera un tiquet amb tota la informació de la venta igual a "ticketWithChange3"

  Escenari: Calcular canvi a tornar quan el client paga amb efectiu insuficient
    Donat afegeixo el producte de codi de barres 123 a la venta
    Donat afegeixo el producte de codi de barres 456 a la venta
    Donat afegeixo el producte de codi de barres 789 a la venta
    Quan el client vol pagar amb "Efectiu" i que el client aporta 34,96€ i vull calcular el canvi a tornar
    Aleshores el canvi a tornar de la venta ha sigut 0,01€
    I es genera un tiquet amb tota la informació de la venta igual a "ticketWithChange4"