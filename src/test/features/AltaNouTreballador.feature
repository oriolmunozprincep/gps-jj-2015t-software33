# language: ca

#noinspection SpellCheckingInspection
Característica: Donar d' alta un nou treballador

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "123"
    I que hi ha un cap de botiga "CapBotiga" amb contrassenya "123"

  Escenari: Registrar un treballador correctament
    Donat que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou treballador amb nom "Treballador1" i contrasenya "123"
    Aleshores S'ha donat d'alta un nou treballador amb nom "Treballador1" i contrasenya "123"

  Escenari: Registrar un cap de botiga correctament
    Donat que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou cap de botiga amb nom "CapBotiga1" i contrasenya "123"
    Aleshores S'ha donat d'alta un nou cap de botiga  amb nom "CapBotiga1" i contrasenya "123"

  Escenari: Registrar un administrador correctament
    Donat que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou administrador amb nom "Jefe2" i contrasenya "123"
    Aleshores S'ha donat d'alta un nou administrador amb nom "Jefe2" i contrasenya "123"

  Escenari: Registrar un treballador sense nom
    Donat que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou treballador amb nom "" i contrasenya "123"
    Aleshores obtinc un error que diu: "Has d'introduir un nom d'usuari"

  Escenari: Registrar un treballador sense contrasenya
    Donat que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou treballador amb nom "Treballador1" i contrasenya ""
    Aleshores obtinc un error que diu: "Has d'introduir una contrasenya"

  Escenari: Registrar un treballador amb el mateix nom que un altre
    Donat que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou treballador amb nom "Joan" i contrasenya "123"
    Aleshores obtinc un error que diu: "Ja existeix un usuari amb aquest username"

  Escenari: Un treballador no pot registrar un administrador
    Donat que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou administrador amb nom "Jefe2" i contrasenya "123"
    Aleshores obtinc un error que diu: "Només els administradors poden afegir nous administradors"

  Escenari: Un treballador no pot registrar un cap de botiga
    Donat que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou cap de botiga amb nom "CapBotiga1" i contrasenya "123"
    Aleshores obtinc un error que diu: "Només els administradors poden afegir nous caps de botiga"

  Escenari: Un cap de botiga pot donar d'alta un nou treballdor
    Donat que en "CapBotiga" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou treballador amb nom "Martí" i contrasenya "123"
    Aleshores S'ha donat d'alta un nou treballador amb nom "Martí" i contrasenya "123"

  Escenari: Un cap de botiga no pot donar d'alta un nou administrador
    Donat que en "CapBotiga" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou administrador amb nom "Jefe2" i contrasenya "123"
    Aleshores obtinc un error que diu: "Només els administradors poden afegir nous administradors"

  Escenari: Un cap de botiga no pot registrar un altre cap de botiga
    Donat que en "CapBotiga" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull donar d'alta un nou cap de botiga amb nom "CapBotiga1" i contrasenya "123"
    Aleshores obtinc un error que diu: "Només els administradors poden afegir nous caps de botiga"