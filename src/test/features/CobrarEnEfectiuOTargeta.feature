# language: ca

#noinspection SpellCheckingInspection
Característica: Seleccionar métode de pagament

  Rerefons:
    Donat que estem al tpv número 2 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Rafa Lucena" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Playmobil", preu 5,00€, iva 0% i codi de barres 126
    I va fer logout
    I que en "Rafa Lucena" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 500€
    I inicio una nova venta

  Escenari: Cobrar una venta en metalic
    Donat afegeixo el producte de codi de barres 126 a la venta
    Quan el client vol pagar amb "Efectiu" i que el client aporta 8,00 € i vull calcular el canvi a tornar
    Aleshores el canvi a tornar de la venta ha sigut 3,00€
    I la venta es en "Efectiu" i està enregistrada

  Escenari: Cobrar una venta amb targeta
    Donat afegeixo el producte de codi de barres 126 a la venta
    Quan el client vol pagar amb "Targeta" i vull calcular el canvi a tornar
    Aleshores el canvi a tornar de la venta ha sigut 0,00€
    I la venta es en "Targeta" i està enregistrada
