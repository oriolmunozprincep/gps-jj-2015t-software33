# language: ca

#noinspection SpellCheckingInspection
Característica: Donar d'alta un val de descompte

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "RobertoAgustinSanPedro" i contrasenya "123"
    I que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 1000€
    I que hi ha una venta iniciada amb data "6/6/666" i hora "7:06"

  Escenari: Val de descompte sense dates
    Quan vull donar de alta un val de descompte de 1€ amb pagament minim de 3€ sense data indicada i amb codi de barres 123
    Aleshores s'ha donat de alta un val de descompte de 1€ amb pagament minim de 3€ sense data indicada i amb codi de barres 123

  Escenari: Val de descompte amb dates
    Quan vull donar de alta un val de descompte de 1€ amb pagament minim de 3€ amb data inici "1/1/2015" i data fi "1/1/2017" i amb codi de barres 123
    Aleshores s'ha donat de alta un val de descompte de 1€ amb pagament minim de 3€ amb data inici "1/1/2015" i data fi "1/1/2017" i amb codi de barres 123

  Escenari: Val de descompte amb quantitat de descompte incorrecta
    Quan vull donar de alta un val de descompte de 0€ amb pagament minim de 3€ sense data indicada i amb codi de barres 123
    Aleshores obtinc un error que diu: "El descompte a de ser major a 0"

  Escenari: Val de descompte amb quantitat de descompte incorrecta
    Quan vull donar de alta un val de descompte de 1€ amb pagament minim de 0€ sense data indicada i amb codi de barres 123
    Aleshores obtinc un error que diu: "El pagament minim a de ser major a 0"

  Escenari: Val de descompte amb quantitat de descompte incorrecta
    Quan vull donar de alta un val de descompte de 5€ amb pagament minim de 1€ sense data indicada i amb codi de barres 123
    Aleshores obtinc un error que diu: "El pagament minim a de ser més gran que el descompte"


  Escenari: Val de descompte amb un codi de barres ja existent
    Donat que el cap ha introduit un val de descompte amb 2€ de descompte per a compres superiors a 5€ sense dates amb codi de barres 123
    Quan vull donar de alta un val de descompte de 3€ amb pagament minim de 7€ sense data indicada i amb codi de barres 123
    Aleshores obtinc un error que diu: "Ja hi ha un val de descompte amb aquest codi de barres"

