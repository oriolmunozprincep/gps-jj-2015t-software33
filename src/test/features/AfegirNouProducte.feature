# language: ca

#noinspection SpellCheckingInspection
Característica: Donar d' alta un nou producte

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un cap de botiga "CapB" amb contrassenya "123"
    I que hi ha un treballador amb nom d'usuari "RobertoAgustinSanPedro" i contrasenya "123"
    I que en "CapB" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 1000€
    I un producte amb nom "asd", marca "qwerty", preu 11€, iva 23% i codi de barres 789456
    I un descompte del tipus "Fix" de "1 €" amb el numero 1 per la marca "qwerty" sense data indicada
    I es va tancar la jornada indicant que hi han 1000€ a caixa
    I va fer logout

  Escenari: Un treballador no pot donar d'alta un nou producte
    Donat que en "RobertoAgustinSanPedro" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull afegir un producte amb codi de barres 123
    Aleshores obtinc un error que diu: "Només els caps de botiga poden afegir nous productes"

  Escenari: Un treballador no pot modificar els productes
    Donat que en "RobertoAgustinSanPedro" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan al producte amb codi de barres 895623 vull posar-l'hi el preu 41,25€
    Aleshores obtinc un error que diu: "Només els caps de botiga poden modificar els productes"

  Escenari: Registrar un producte sense marca correctament
    Donat que en "CapB" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull afegir un producte amb nom "test", preu 8€, iva 21% i codi de barres 123456
    Aleshores hi ha un producte amb nom "test", preu 8€, iva 21% i codi de barres 123456

  Escenari: Registrar un producte amb marca correctament
    Donat que en "CapB" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull afegir un producte amb nom "test2", marca "qwerty" preu 58€, iva 15% i codi de barres 654321
    Aleshores hi ha un producte amb nom "test2", marca "qwerty" preu 58€, iva 15% i codi de barres 654321

  Escenari: Registrar producte i veure que es al descompte per marca que correspon
    Donat que en "CapB" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull afegir un producte amb nom "test3", marca "qwerty" preu 88€, iva 12% i codi de barres 456789
    Aleshores el descompte 1 fara descompte al producte amb codi de barres 456789

  Escenari: Registrar un producte nomes amb el codi de barres
    Donat que en "CapB" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull afegir un producte amb codi de barres 741258
    Aleshores hi ha un producte amb codi de barres 741258

  Escenari: Registrar un producte nomes amb el codi de barres i afegin a ma la resta d'atributs
    Donat que en "CapB" ha iniciat la sessio al tpv amb contrasenya "123"
    Donat que vull afegir un producte amb codi de barres 895623
    Quan al producte amb codi de barres 895623 vull posar-l'hi el preu 41,25€
    I al producte amb codi de barres 895623 vull posar-l'hi iva del 8%
    I al producte amb codi de barres 895623 vull posar-l'hi el nom "test4"
    Aleshores hi ha un producte amb nom "test4", preu 41,25€, iva 8% i codi de barres 895623

  Escenari: Registrar un producte amb un codi de barres ja existent
    Donat que en "CapB" ha iniciat la sessio al tpv amb contrasenya "123"
    Donat un producte amb nom "Hey", preu 2€, iva 12% i codi de barres 123
    Quan vull afegir un producte amb nom "Repe", marca "Bu" preu 12€, iva 21% i codi de barres 123
    Aleshores obtinc un error que diu: "Ja existeix un producte amb aquest codi de barres"