# language: ca

#noinspection SpellCheckingInspection
Característica: Iniciar la jornada

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "123"
    I que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"

  Escenari: Inici de torn
    Quan inicio un nou torn amb 567,8€
    Aleshores El nou torn iniciat te una caixa inicial del 567,8€

  Escenari: Error si intentem iniciar un torn amb menys de 50€
    Quan inicio un nou torn amb 30€
    Aleshores obtinc un error que diu: "No pots iniciar un torn amb menys de 50€"


  Escenari:Ja hi ha un torn iniciat
    Donat que s'ha iniciat el torn el dia "18/12/2015 10:00" amb 500€
    Quan intento tornar a iniciar el dia "19/12/2015 15:00" amb 532,40€
    Aleshores obtinc un error que diu: "Ja hi ha un torn iniciat"

  Escenari:Torns diferents
    Donat que s'ha iniciat el torn el dia "18/12/2015 15:00" amb 500€
    I tanco el torn indicant que hi han 500€ a caixa
    Quan intento tornar a iniciar el dia "18/12/2015 18:00" amb 532,40€
    Aleshores no sortira cap missatge d'error

  Escenari:Iniciar venta sense torn iniciat
    Quan inicio una nova venta
    Aleshores obtinc un error que diu: "No s'ha iniciat un torn"

   Escenari: Afegim un producte amb codi de barres sense jornada
     Donat un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
     Quan afegeixo el producte de codi de barres 1234567 a la venta
     Aleshores obtinc un error que diu: "No s'ha iniciat un torn"




