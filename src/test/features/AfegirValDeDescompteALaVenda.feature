#language: ca

#noinspection SpellCheckingInspection
Característica: Afegir un val de descompte a la venda

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Playmobil", marca "aDDoS", preu 5,00€, iva 0% i codi de barres 123
    I un producte amb nom "Playmobil2", marca "aDDoS", preu 5,20€, iva 0% i codi de barres 1232
    I un producte amb nom "Playmobil3", preu 5,30€, iva 0% i codi de barres 1233
    I va fer logout
    I que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 1000€
    I que hi ha una venta iniciada amb data "6/6/666" i hora "7:06"

  Escenari: Afegir un val de descompte a la venta que superi el minim a comprar per aplicar-lo sense dates
    Donat afegeixo el producte de codi de barres 123 a la venta
    I que el cap ha introduit un val de descompte amb 1€ de descompte per a compres superiors a 3€ sense dates amb codi de barres 123
    Quan el client entrega un val de descompte amb codi de barres 123
    Aleshores el total de la venta actual és de 4€


  Escenari: Afegir un val de descompte a la venta que superi el minim a comprar per aplicar-lo sense dates
    Donat afegeixo el producte de codi de barres 123 a la venta
    I que el cap ha introduit un val de descompte amb 1€ de descompte per a compres superiors a 3€ sense dates amb codi de barres 123
    I el client entrega un val de descompte amb codi de barres 123
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticketWithVoucher"

  Escenari: Afegir un val de descompte a la venta que superi el minim a comprar per aplicar-lo amb dates
    Donat afegeixo el producte de codi de barres 123 a la venta
    I que el cap ha introduit un val de descompte amb 1€ de descompte per a compres superiors a 3€ amb data inici "1/1/2015" i data fi "1/1/2017" amb codi de barres 123
    Quan el client entrega un val de descompte amb codi de barres 123
    Aleshores el total de la venta actual és de 4€

  Escenari: Afegir un val de descompte a la venta que no superi el minim a comprar per aplicar-lo sense dates
    Donat afegeixo el producte de codi de barres 123 a la venta
    I que el cap ha introduit un val de descompte amb 1€ de descompte per a compres superiors a 6€ sense dates amb codi de barres 123
    Quan el client entrega un val de descompte amb codi de barres 123
    Aleshores obtinc un error que diu: "No es aplicable amb aquest cost de compra"

  Escenari: Afegir un val de descompte a la venta que no superi el minim a comprar per aplicar-lo amb dates
    Donat afegeixo el producte de codi de barres 123 a la venta
    I que el cap ha introduit un val de descompte amb 1€ de descompte per a compres superiors a 6€ amb data inici "1/1/2015" i data fi "1/1/2017" amb codi de barres 123
    Quan el client entrega un val de descompte amb codi de barres 123
    Aleshores obtinc un error que diu: "No es aplicable amb aquest cost de compra"

  Escenari: Afegir un val de descompte a la venta que no sigui correcte sense dates
    Donat afegeixo el producte de codi de barres 123 a la venta
    I que el cap ha introduit un val de descompte amb 1€ de descompte per a compres superiors a 6€ sense dates amb codi de barres 123
    Quan el client entrega un val de descompte amb codi de barres 124
    Aleshores obtinc un error que diu: "No es un val de descompte correcte"

  Escenari: Afegir un val de descompte inexistent a una venda
    Donat afegeixo el producte de codi de barres 123 a la venta
    I que el cap ha introduit un val de descompte amb 1€ de descompte per a compres superiors a 6€ amb data inici "1/1/1999" i data fi "1/1/2000" amb codi de barres 123
    Quan el client entrega un val de descompte amb codi de barres 124
    Aleshores obtinc un error que diu: "No es un val de descompte correcte"

  Escenari: Afegir un val de descompte a la venta caducat
    Donat afegeixo el producte de codi de barres 123 a la venta
    I que el cap ha introduit un val de descompte amb 1€ de descompte per a compres superiors a 6€ amb data inici "1/1/1999" i data fi "1/1/2000" amb codi de barres 123
    Quan el client entrega un val de descompte amb codi de barres 123
    Aleshores obtinc un error que diu: "Val caducat"
    
  