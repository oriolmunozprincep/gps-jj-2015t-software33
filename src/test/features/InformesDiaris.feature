# language: ca

#noinspection SpellCheckingInspection
Característica: Informes Diaris

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "123"
    I que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"

    Escenari: Consultar informes de un dia
      Donat que s'ha iniciat el torn el dia "18/12/2015 10:00" amb 500€
      I es va tancar la jornada indicant que hi han 505,24€ a caixa
      Aleshores La consulta del dia retorna un desquadrament de 5,24€ el dia "18/12/2015 10:00" amb el venedor "Joan" al tpv 1 i sense comentari
