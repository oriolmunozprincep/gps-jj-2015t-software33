# language: ca

#noinspection SpellCheckingInspection
Característica: Alta de descompte

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "RobertoAgustinSanPedro" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Playmobil", marca "aDDos", preu 5,00€, iva 0% i codi de barres 123
    I un producte amb nom "Playmobil2", marca "aDDos", preu 5,20€, iva 0% i codi de barres 1232
    I un producte amb nom "Playmobil3", preu 5,30€, iva 0% i codi de barres 1233
    I va fer logout
    I que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 1000€
    I que hi ha una venta iniciada amb data "6/6/666" i hora "7:06"

  ##Descompte fix ##

  Escenari: Descompte fix per 1 producte sense dates
    Quan vull donar de alta un descompte del tipus "Fix" de "1 €" per al producte "Playmobil" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Fix" de "1 €" per al producte "Playmobil" sense data indicada

  Escenari: Descompte fix per 1 producte amb dates
    Quan vull donar de alta un descompte del tipus "Fix" de "1 €" per al producte "Playmobil" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Fix" de "1 €" per al producte "Playmobil" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte fix per més de 1 producte sense dates
    Quan vull donar de alta un descompte del tipus "Fix" de "1 €" per als productes "discountProducts" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Fix" de "1 €" per als productes "discountProducts" sense data indicada

  Escenari: Descompte fix per més de 1 producte amb dates
    Quan vull donar de alta un descompte del tipus "Fix" de "1 €" per als productes "discountProducts" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Fix" de "1 €" per als productes "discountProducts" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte fix per una marca sense dates
    Quan vull donar de alta un descompte del tipus "Fix" de "1 €" per la marca "aDDos" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Fix" de "1 €" per la marca "aDDos" sense data indicada

  Escenari: Descompte fix per una marca amb dates
    Quan vull donar de alta un descompte del tipus "Fix" de "1 €" per la marca "aDDos" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Fix" de "1 €" per la marca "aDDos" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte fix per a tota la venta sense dates
    Quan vull donar de alta un descompte del tipus "Fix" de "1 €" per a tota la venta sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Fix" de "1 €" per a tota la venta sense data indicada

  Escenari: Descompte fix per a tota la venta amb dates
    Quan vull donar de alta un descompte del tipus "Fix" de "1 €" per a tota la venta amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Fix" de "1 €" per a tota la venta amb data inici "1/1/2015" i data fi "1/1/2017"

  ##Descompte percentual ##

  Escenari: Descompte percentual per 1 producte sense dates
    Quan vull donar de alta un descompte del tipus "Percentual" de "10 %" per al producte "Playmobil" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Percentual" de "10 %" per al producte "Playmobil" sense data indicada

  Escenari: Descompte percentual per 1 producte amb dates
    Quan vull donar de alta un descompte del tipus "Percentual" de "10 %" per al producte "Playmobil" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Percentual" de "10 %" per al producte "Playmobil" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte percentual per més de 1 producte sense dates
    Quan vull donar de alta un descompte del tipus "Percentual" de "10 %" per als productes "discountProducts" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Percentual" de "10 %" per als productes "discountProducts" sense data indicada

  Escenari: Descompte percentual per més de 1 producte amb dates
    Quan vull donar de alta un descompte del tipus "Percentual" de "10 %" per als productes "discountProducts" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Percentual" de "10 %" per als productes "discountProducts" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte percentual per una marca sense dates
    Quan vull donar de alta un descompte del tipus "Percentual" de "10 %" per la marca "aDDos" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Percentual" de "10 %" per la marca "aDDos" sense data indicada

  Escenari: Descompte percentual per una marca amb dates
    Quan vull donar de alta un descompte del tipus "Percentual" de "10 %" per la marca "aDDos" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Percentual" de "10 %" per la marca "aDDos" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte percentual per a tota la venta sense dates
    Quan vull donar de alta un descompte del tipus "Percentual" de "10 %" per a tota la venta sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Percentual" de "10 %" per a tota la venta sense data indicada

  Escenari: Descompte percentual per a tota la venta amb dates
    Quan vull donar de alta un descompte del tipus "Percentual" de "10 %" per a tota la venta amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Percentual" de "10 %" per a tota la venta amb data inici "1/1/2015" i data fi "1/1/2017"

  ##Descompte N x M ##

  Escenari: Descompte N x M per 1 producte sense dates
    Quan vull donar de alta un descompte del tipus "NxM" de "3x2" per al producte "Playmobil" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "NxM" de "3x2" per al producte "Playmobil" sense data indicada

  Escenari: Descompte N x M per 1 producte amb dates
    Quan vull donar de alta un descompte del tipus "NxM" de "3x2" per al producte "Playmobil" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "NxM" de "3x2" per al producte "Playmobil" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte N x M per més de 1 producte sense dates
    Quan vull donar de alta un descompte del tipus "NxM" de "3x2" per als productes "discountProducts" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "NxM" de "3x2" per als productes "discountProducts" sense data indicada

  Escenari: Descompte N x M per més de 1 producte amb dates
    Quan vull donar de alta un descompte del tipus "NxM" de "3x2" per als productes "discountProducts" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "NxM" de "3x2" per als productes "discountProducts" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte N x M per una marca sense dates
    Quan vull donar de alta un descompte del tipus "NxM" de "3x2" per la marca "aDDos" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "NxM" de "3x2" per la marca "aDDos" sense data indicada

  Escenari: Descompte N x M per una marca amb dates
    Quan vull donar de alta un descompte del tipus "NxM" de "3x2" per la marca "aDDos" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "NxM" de "3x2" per la marca "aDDos" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte N x M per a tota la venta sense dates
    Quan vull donar de alta un descompte del tipus "NxM" de "3x2" per a tota la venta sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "NxM" de "3x2" per a tota la venta sense data indicada

  Escenari: Descompte N x M per a tota la venta amb dates
    Quan vull donar de alta un descompte del tipus "NxM" de "3x2" per a tota la venta amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "NxM" de "3x2" per a tota la venta amb data inici "1/1/2015" i data fi "1/1/2017"


  ##Descompte Regal ##

  Escenari: Descompte regal per 1 producte sense dates
    Quan vull donar de alta un descompte del tipus "Regal" de "giftedProducts" per al producte "Playmobil" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Regal" de "giftedProducts" per al producte "Playmobil" sense data indicada

  Escenari: Descompte regal per 1 producte amb dates
    Quan vull donar de alta un descompte del tipus "Regal" de "giftedProducts" per al producte "Playmobil" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Regal" de "giftedProducts" per al producte "Playmobil" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte regal per més de 1 producte sense dates
    Quan vull donar de alta un descompte del tipus "Regal" de "giftedProducts" per als productes "discountProducts" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Regal" de "giftedProducts" per als productes "discountProducts" sense data indicada

  Escenari: Descompte regal per més de 1 producte amb dates
    Quan vull donar de alta un descompte del tipus "Regal" de "giftedProducts" per als productes "discountProducts" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Regal" de "giftedProducts" per als productes "discountProducts" amb data inici "1/1/2015" i data fi "1/1/2017"

  Escenari: Descompte regal per una marca sense dates
    Quan vull donar de alta un descompte del tipus "Regal" de "giftedProducts" per la marca "aDDos" sense data indicada
    Aleshores s'ha donat de alta el descompte del tipus "Regal" de "giftedProducts" per la marca "aDDos" sense data indicada

  Escenari: Descompte regal per una marca amb dates
    Quan vull donar de alta un descompte del tipus "Regal" de "giftedProducts" per la marca "aDDos" amb data inici "1/1/2015" i data fi "1/1/2017"
    Aleshores s'ha donat de alta el descompte del tipus "Regal" de "giftedProducts" per la marca "aDDos" amb data inici "1/1/2015" i data fi "1/1/2017"

