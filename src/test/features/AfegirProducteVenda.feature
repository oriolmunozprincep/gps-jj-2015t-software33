# language: ca

#noinspection SpellCheckingInspection
Característica: Afegir producte a una venda

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
    I un producte amb nom "Second", preu 19,9€, iva 15% i codi de barres 98765
    I un producte amb nom "Third", preu 0,5€, iva 15% i codi de barres 654321
    I va fer logout
    I que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 500€

  Escenari: Iniciar venda amb Producte
    Quan afegeixo el producte de codi de barres 1234567 a la venta
    Aleshores la venta actual és de'n "Joan" al tpv 1 de la botiga "Girona 1"
    I la venta té 1 línia
    I línia de venta 1 és de 1 unitats de "Optimus Prime" a 27,83€ cada una per un total de 27,83€
    I el total de la venta actual és de 27,83€
    I el dia de la venda és l'actual

  Escenari: Afegir el primer producte d'una venda existent
    Donat inicio una nova venta
    Quan afegeixo el producte de codi de barres 1234567 a la venta
    Aleshores la venta té 1 línia
    I línia de venta 1 és de 1 unitats de "Optimus Prime" a 27,83€ cada una per un total de 27,83€
    I el total de la venta actual és de 27,83€
    I el dia de la venda és l'actual
    I l'iva del producte de la línia 1 és del 21%
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 27.83€/u x 1u = 27.83€
    ---
    Total: 27.83€
    """

  Escenari: Afegir un segon producte amb preu decimal
    Donat inicio una nova venta
    Donat afegeixo el producte de codi de barres 1234567 a la venta
    Quan afegeixo el producte de codi de barres 98765 a la venta
    Aleshores  la venta actual és de'n "Joan" al tpv 1 de la botiga "Girona 1"
    I la venta té 2 línies
    I línia de venta 1 és de 1 unitats de "Optimus Prime" a 27,83€ cada una per un total de 27,83€
    I línia de venta 2 és de 1 unitats de "Second" a 22,885€ cada una per un total de 22,885€
    I el total de la venta actual és de 50,72€
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 27.83€/u x 1u = 27.83€
    Second - 22.89€/u x 1u = 22.89€
    ---
    Total: 50.72€
    """

  Escenari: Afegir un sol producte repetit
    Donat inicio una nova venta
    I afegeixo el producte de codi de barres 1234567 a la venta
    Quan afegeixo el producte de codi de barres 1234567 a la venta
    Aleshores la venta té 1 línia
    I línia de venta 1 és de 2 unitats de "Optimus Prime" a 27,83€ cada una per un total de 55,66€
    I el total de la venta actual és de 55,66€
    I el dia de la venda és l'actual
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 27.83€/u x 2u = 55.66€
    ---
    Total: 55.66€
    """

  Escenari: Afegir tres productes i repetir un d'ells
    Donat inicio una nova venta
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 1234567 a la venta
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 98765 a la venta
    Quan afegeixo el producte de codi de barres 98765 a la venta
    Aleshores  la venta actual és de'n "Joan" al tpv 1 de la botiga "Girona 1"
    I la venta té 3 línies
    I línia de venta 1 és de 2 unitats de "Third" a 0,575€ cada una per un total de 1,15€
    I línia de venta 2 és de 1 unitats de "Optimus Prime" a 27,83€ cada una per un total de 27,83€
    I línia de venta 3 és de 2 unitats de "Second" a 22,885€ cada una per un total de 45,77€
    I l'iva del producte de la línia 1 és del 15%
    I l'iva del producte de la línia 2 és del 21%
    I l'iva del producte de la línia 3 és del 15%
    I el total de la venta actual és de 74,75€
    I la pantalla del client del tpv mostra
    """
    Third - 0.57€/u x 2u = 1.15€
    Optimus Prime - 27.83€/u x 1u = 27.83€
    Second - 22.89€/u x 2u = 45.77€
    ---
    Total: 74.75€
    """

  Escenari: Modificar la quantitat d'una linia
    Donat inicio una nova venta
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 98765 a la venta
    Quan modifico la quantitat de la línia 2 a 4 unitats
    Aleshores la venta actual és de'n "Joan" al tpv 1 de la botiga "Girona 1"
    I la venta té 2 línies
    I línia de venta 1 és de 2 unitats de "Third" a 0,575€ cada una per un total de 1,15€
    I línia de venta 2 és de 4 unitats de "Second" a 22,885€ cada una per un total de 91,54€
    I l'iva del producte de la línia 1 és del 15%
    I l'iva del producte de la línia 2 és del 15%
    I el total de la venta actual és de 92,69€
    I la pantalla del client del tpv mostra
    """
    Third - 0.57€/u x 2u = 1.15€
    Second - 22.89€/u x 4u = 91.54€
    ---
    Total: 92.69€
    """

  Escenari: Modificar la quantitat d'un producte per codi de barres ja existent a la venda
    Donat inicio una nova venta
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 98765 a la venta
    Quan modifico la quantitat del producte amb codi de barres 98765 a 4 unitats
    Aleshores la venta actual és de'n "Joan" al tpv 1 de la botiga "Girona 1"
    I la venta té 2 línies
    I línia de venta 1 és de 2 unitats de "Third" a 0,575€ cada una per un total de 1,15€
    I línia de venta 2 és de 4 unitats de "Second" a 22,885€ cada una per un total de 91,54€
    I l'iva del producte de la línia 1 és del 15%
    I l'iva del producte de la línia 2 és del 15%
    I el total de la venta actual és de 92,69€
    I la pantalla del client del tpv mostra
    """
    Third - 0.57€/u x 2u = 1.15€
    Second - 22.89€/u x 4u = 91.54€
    ---
    Total: 92.69€
    """

  Escenari: Modificar la quantitat d'un producte per codi de barres no existent a la venda
    Donat inicio una nova venta
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 654321 a la venta
    I afegeixo el producte de codi de barres 98765 a la venta
    Quan modifico la quantitat del producte amb codi de barres 1234567 a 4 unitats
    Aleshores la venta actual és de'n "Joan" al tpv 1 de la botiga "Girona 1"
    I la venta té 3 línies
    I línia de venta 1 és de 2 unitats de "Third" a 0,575€ cada una per un total de 1,15€
    I línia de venta 2 és de 1 unitats de "Second" a 22,885€ cada una per un total de 22,885€
    I línia de venta 3 és de 4 unitats de "Optimus Prime" a 27,83€ cada una per un total de 111,32€
    I l'iva del producte de la línia 1 és del 15%
    I l'iva del producte de la línia 2 és del 15%
    I el total de la venta actual és de 135,35€
    I la pantalla del client del tpv mostra
    """
    Third - 0.57€/u x 2u = 1.15€
    Second - 22.89€/u x 1u = 22.89€
    Optimus Prime - 27.83€/u x 4u = 111.32€
    ---
    Total: 135.35€
    """


  Escenari: Afegir un producte sense codi de barres
    Donat inicio una nova venta
    Donat que el tpv mostra la llista de productes "SaleProductList1"
    Quan afegeixo el producte de la linia 1
    Aleshores  la venta té 1 línia
    I línia de venta 1 és de 1 unitats de "Optimus Prime" a 27,83€ cada una per un total de 27,83€
    I el total de la venta actual és de 27,83€
    I el dia de la venda és l'actual
    I l'iva del producte de la línia 1 és del 21%
    I la pantalla del client del tpv mostra
    """
    Optimus Prime - 27.83€/u x 1u = 27.83€
    ---
    Total: 27.83€
    """
