# language: ca

#noinspection SpellCheckingInspection
Característica: Aplicar el millor descompte en un conjunt de productes

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Prime", preu 23€, iva 21% i codi de barres 0123456
    I un producte amb nom "Second", marca "LEG", preu 18€, iva 21% i codi de barres 6543210
    I un producte amb nom "Third", marca "LEG", preu 2€, iva 21% i codi de barres 98765
    I un producte amb nom "Fourth", marca "PLAY", preu 20€, iva 21% i codi de barres 56789
    I un producte amb nom "somnyMony", marca "PLAY", preu 20€, iva 21% i codi de barres 56791
    I un producte amb nom "sonyNoMola", marca "PLAY", preu 10€, iva 21% i codi de barres 56790
    I va fer logout
    I que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 500€
    I que hi ha un descompte percentual del 21% sobre el producte "Fourth"
    I que hi ha un descompte percentual del 5% sobre el producte "Fourth"


  Escenari: donat dos descomptes percentuals, escull el més gran
    Quan afegeixo el producte de codi de barres 56789 a la venta
    Aleshores la venta té 1 línia
    I el total de la venta actual és de 19,12€

  Escenari: donat dos descomptes percentuals i un descompte fixe en la venda, escull el millor
    Donat que hi ha un descompte fixe de 10€ sobre la venta
    Quan afegeixo el producte de codi de barres 56789 a la venta
    Aleshores la venta té 1 línia
    I el total de la venta actual és de 14,2€

  Escenari: donat que hi ha dos descomptes fixes en la venda i dos descomptes percentuals en PLAY, escull el millor
    Donat que hi ha un descompte fixe de 10€ sobre la venta
    I que hi ha un altre descompte fixe de 15€ sobre la venta
    Quan afegeixo el producte de codi de barres 56789 a la venta
    Aleshores la venta té 1 línia
    I el total de la venta actual és de 9,2€

  Escenari: donat un 2x1 en PLAY i descomptes percentuals en PLAY, escull el millor
    Donat que hi ha un 2x1 en "PLAY"
    Quan afegeixo el producte de codi de barres 56789 a la venta
    I afegeixo el producte de codi de barres 56789 a la venta
    Aleshores la venta té 1 línia
    I el total de la venta actual és de 24,2€

  Escenari: donat un 2x1 en PLAY, dos descomptes percentuals en PLAY i un descompte fixe, escull el millor
    Donat que hi ha un 2x1 en "PLAY"
    I que hi ha un descompte fixe de 30€ sobre la venta
    Quan afegeixo el producte de codi de barres 56789 a la venta
    I afegeixo el producte de codi de barres 56789 a la venta
    Aleshores la venta té 1 línia
    I el total de la venta actual és de 18,4€

  Escenari: donat dos descomptes percentuals i un regal
    Donat que per la compra d'un producte "Fourth" es regala una unitat del producte "Third"
    I afegeixo el producte de codi de barres 98765 a la venta
    Quan afegeixo el producte de codi de barres 56789 a la venta
    Aleshores la venta té 2 línia
    I el total de la venta actual és de 19,12€


  Escenari: donat un 2x1 en la marca PLAY
    Donat que hi ha un 2x1 en "PLAY"
    Quan afegeixo el producte de codi de barres 56791 a la venta
    I afegeixo el producte de codi de barres 56790 a la venta
    Aleshores la venta té 2 línia
    I el total de la venta actual és de 24,2€


  Escenari: donat un 3x2 en la marca PLAY
    Donat que hi ha un 3x2 en "PLAY"
    Quan afegeixo el producte de codi de barres 56791 a la venta
    I afegeixo el producte de codi de barres 56791 a la venta
    I afegeixo el producte de codi de barres 56790 a la venta
    I afegeixo el producte de codi de barres 56790 a la venta
    Aleshores la venta té 2 línia
    I el total de la venta actual és de 60,5€

