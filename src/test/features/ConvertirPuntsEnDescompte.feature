# language: ca

#noinspection SpellCheckingInspection
Característica: Convertir els punts d'un client fidelitzat en un descompte

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "RobertoAgustinSanPedro" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Playmobil", preu 2,00€, iva 0% i codi de barres 123
    I un producte amb nom "Playmobil2", preu 2,50€, iva 0% i codi de barres 456
    I va fer logout
    I que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 1000€
    I que hi ha una venta iniciada amb data "25/11/2015" i hora "10:00"

  Escenari: El client fidelitzat guanya punts al realitzar una compra
    Donat afegeixo el producte de codi de barres 123 a la venta
    I un client fidelitzat amb nom "Reaper" i numero de telefon "666666666"
    I el client s'identifica com a fidelitzat donant el numero de telefon "666666666"
    Quan el client vol pagar amb "Efectiu" i que el client aporta 2,00€ i vull calcular el canvi a tornar
    Aleshores el client fidelitzat amb numero de telefon "666666666" té 200 punts

  Escenari: El client no te un numero de telefon registrat
    Donat afegeixo el producte de codi de barres 123 a la venta
    Quan el client s'identifica com a fidelitzat donant el numero de telefon "111111111"
    Aleshores obtinc un error que diu: "No existeix cap client fidelitzat amb aquest numero de telefon"

  Escenari: El client te 1000 punts canviables a 1€
    Donat afegeixo el producte de codi de barres 123 a la venta
    I un client fidelitzat amb nom "Jari", numero de telefon "777777777" i 1000 punts
    I el client s'identifica com a fidelitzat donant el numero de telefon "777777777"
    Quan el client vol pagar amb "Efectiu" i que el client aporta 2,00€ i vull calcular el canvi a tornar
    Aleshores el client fidelitzat amb numero de telefon "777777777" té 100 punts
    I el canvi a tornar de la venta ha sigut 1€

  Escenari: El client te 1500 punts canviables a 1,5€
    Donat afegeixo el producte de codi de barres 456 a la venta
    I un client fidelitzat amb nom "Jari", numero de telefon "777777777" i 1500 punts
    I el client s'identifica com a fidelitzat donant el numero de telefon "777777777"
    Quan el client vol pagar amb "Efectiu" i que el client aporta 3,00€ i vull calcular el canvi a tornar
    Aleshores el client fidelitzat amb numero de telefon "777777777" té 100 punts
    I el canvi a tornar de la venta ha sigut 2€

  Escenari: El client no fa el minim de 1€ de compra
    Donat afegeixo el producte de codi de barres 123 a la venta
    I un client fidelitzat amb nom "Jari", numero de telefon "777777777" i 2000 punts
    I el client s'identifica com a fidelitzat donant el numero de telefon "777777777"
    Quan el client vol pagar amb "Efectiu" i que el client aporta 0,00€ i vull calcular el canvi a tornar
    Aleshores obtinc un error que diu: "El client ha de aportar un minim de 1€"

  Escenari: El client fidelitzat paga amb targeta i te punts suficients
    Donat afegeixo el producte de codi de barres 123 a la venta
    I un client fidelitzat amb nom "Jari", numero de telefon "777777777" i 20000 punts
    I el client s'identifica com a fidelitzat donant el numero de telefon "777777777"
    Quan el client vol pagar amb "Targeta" i que el client aporta 0,00€ i vull calcular el canvi a tornar
    Aleshores el client fidelitzat amb numero de telefon "777777777" té 0 punts

  Escenari: El client fidelitzat paga amb targeta i no te punts
    Donat afegeixo el producte de codi de barres 123 a la venta
    I un client fidelitzat amb nom "Jari", numero de telefon "777777777" i 0 punts
    I el client s'identifica com a fidelitzat donant el numero de telefon "777777777"
    Quan el client vol pagar amb "Targeta" i que el client aporta 0,00€ i vull calcular el canvi a tornar
    Aleshores el client fidelitzat amb numero de telefon "777777777" té 200 punts

  Escenari: El client fidelitzat paga amb punts una part de la venta i es genera el ticket corresponent
    Donat afegeixo el producte de codi de barres 123 a la venta
    I un client fidelitzat amb nom "Jari", numero de telefon "777777777" i 1000 punts
    I el client s'identifica com a fidelitzat donant el numero de telefon "777777777"
    Quan el client vol pagar amb "Efectiu" i que el client aporta 1,00€ i vull calcular el canvi a tornar
    Aleshores el client fidelitzat amb numero de telefon "777777777" té 100 punts
    I es genera un tiquet amb tota la informació de la venta igual a "ticketWithLoyalDiscount"