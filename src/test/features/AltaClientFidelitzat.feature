# language: ca

#noinspection SpellCheckingInspection
Característica: Donar d' alta un nou client fidelitzat

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un cap de botiga "CapB" amb contrassenya "123"
    I que hi ha un treballador amb nom d'usuari "RobertoAgustinSanPedro" i contrasenya "123"

  Escenari: Registrar un client fidelitzat correctament
    Donat que en "CapB" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan vull afegir un client fidelitzat amb nom "test" i numero de telefon "999999999"
    Aleshores hi ha un client fidelitzat amb nom "test", numero de telefon "999999999" i amb 0 punts

  Escenari: Registrar un client fidelitzat amb un numero de telefon ja existent
    Donat que en "RobertoAgustinSanPedro" ha iniciat la sessio al tpv amb contrasenya "123"
    Donat un client fidelitzat amb nom "MissingNo" i numero de telefon "999999999"
    Quan vull afegir un client fidelitzat amb nom "test" i numero de telefon "999999999"
    Aleshores obtinc un error que diu: "Ja existeix un client fidelitzat amb aquest numero de telefon"

  Escenari: Registrar un client fidelitzat amb un nom existent pero amb numero diferent
    Donat que en "RobertoAgustinSanPedro" ha iniciat la sessio al tpv amb contrasenya "123"
    Donat un client fidelitzat amb nom "MissingNo" i numero de telefon "999999999"
    Quan vull afegir un client fidelitzat amb nom "MissingNo" i numero de telefon "888888888"
    Aleshores hi ha un client fidelitzat amb nom "MissingNo", numero de telefon "888888888" i amb 0 punts