# language: ca

#noinspection SpellCheckingInspection
Característica: Informe detallat de les vendes diàries

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "First", preu 10€, iva 0% i codi de barres 1
    I un producte amb nom "Second", preu 20€, iva 0% i codi de barres 2
    I va fer logout
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "JoanMVP"
    I que el cap ha introduit un descompte percentual del 100% sobre el producte "First"
    I que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"

    # El contingut de productes d'una venda es troba a /resources/vendes, m'és més còmode

  Escenari: Una venda un dia
    Donat que es va fer la venda 1 el dia "27/05/12" a les "15:38" atesa per "Joan" i pagada amb mètode "Targeta" amb 0 euros de diners donats i amb productes:
      | First  |
      | First  |
      | Second |
    Quan vull generar un informe de totes les vendes
    Aleshores es genera un informe com l'"informe1venda1dia"

  Escenari: Dos vendes un dia
    Donat que es va fer la venda 1 el dia "27/05/12" a les "15:38" atesa per "Joan" i pagada amb mètode "Targeta" amb 0 euros de diners donats i amb productes:
      | First  |
      | First  |
      | Second |
    I que es va fer la venda 2 el dia "27/05/12" a les "17:53" atesa per "Joan" i pagada amb mètode "Efectiu" amb 100 euros de diners donats i amb productes:
      | Second |
      | Second |
      | Second |
    Quan vull generar un informe de totes les vendes
    Aleshores es genera un informe com l'"informe2vendes1dia"

  Escenari: Dos vendes dos dies
    Donat que es va fer la venda 1 el dia "27/05/12" a les "15:38" atesa per "Joan" i pagada amb mètode "Targeta" amb 0 euros de diners donats i amb productes:
      | First  |
      | First  |
      | Second |
    I que es va fer la venda 2 el dia "28/05/12" a les "17:53" atesa per "Joan" i pagada amb mètode "Efectiu" amb 100 euros de diners donats i amb productes:
      | Second |
      | Second |
      | Second |
    Quan vull generar un informe de totes les vendes
    Aleshores es genera un informe com l'"informe2vendes2dies"