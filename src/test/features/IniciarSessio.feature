# language: ca

#noinspection SpellCheckingInspection
Característica: Iniciar sessió a l'inici de la jornada

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "123"
    I que hi ha un cap de botiga "CapB" amb contrassenya "123"
    I que en "CapB" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 1000€
    I un producte amb nom "asd", marca "qwerty", preu 11€, iva 23% i codi de barres 321
    I un descompte del tipus "Fix" de "1 €" amb el numero 1 per la marca "qwerty" sense data indicada
    I es va tancar la jornada indicant que hi han 1000€ a caixa
    I va fer logout

  Escenari: Inici de sessió correcte
    Quan que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"
    Aleshores el tpv està en ús per en "Joan"

  Escenari: Un venedor no pot inicar sessió si ja hi ha un altre venedor al tpv
    Donat que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"
    Quan que en "Jefe" ha iniciat la sessio al tpv amb contrasenya "123"
    Aleshores obtinc un error que diu: "Aquest tpv està en ús per Joan"

  Escenari: Inici de torn sense nom
    Quan que en "" ha iniciat la sessio al tpv amb contrasenya "123"
    Aleshores obtinc un error que diu: "Has d'introduir un nom d'usuari"

  Escenari: Inici de torn d'un usuari no registrat
    Quan que en "Martí" ha iniciat la sessio al tpv amb contrasenya "123"
    Aleshores obtinc un error que diu: "No hi ha cap empleat amb aquest nom"

  Escenari: Inici de torn amb contrasenya incorrecte
    Quan que en "Joan" ha iniciat la sessio al tpv amb contrasenya "789"
    Aleshores obtinc un error que diu: "La contrasenya està malament"
    
  Escenari: No es pot donar d'alta un producte per codi de barres sense haver iniciat sessió
    Quan vull afegir un producte amb codi de barres 123
    Aleshores obtinc un error que diu: "No es poden afegir productes si no s'ha iniciat sessió"

  Escenari: No es pot donar d'alta un producte sense marca haver iniciat sessió
    Quan vull afegir un producte amb nom "Nope", preu 23€, iva 10% i codi de barres 12344567
    Aleshores obtinc un error que diu: "No es poden afegir productes si no s'ha iniciat sessió"

  Escenari: No es pot donar d'alta un producte amb marca haver iniciat sessió
    Quan vull afegir un producte amb nom "Ara", marca "NoWay" preu 12€, iva 8% i codi de barres 9887765
    Aleshores obtinc un error que diu: "No es poden afegir productes si no s'ha iniciat sessió"

  Escenari: No es pot modificar el nom d'un producte sense haver iniciat sessió
    Quan al producte amb codi de barres 321 vull posar-l'hi el nom "prova1"
    Aleshores obtinc un error que diu: "No es poden modificar productes si no s'ha iniciat sessió"

  Escenari: No es pot modificar el preu d'un producte sense haver iniciat sessió
    Quan al producte amb codi de barres 321 vull posar-l'hi el preu 21€
    Aleshores obtinc un error que diu: "No es poden modificar productes si no s'ha iniciat sessió"

  Escenari: No es pot modificar l'iva d'un producte sense haver iniciat sessió
    Quan al producte amb codi de barres 321 vull posar-l'hi iva del 34%
    Aleshores obtinc un error que diu: "No es poden modificar productes si no s'ha iniciat sessió"
    
  Escenari: No es pot iniciar una venta sense haver iniciat sessió
    Quan inicio una nova venta
    Aleshores obtinc un error que diu: "No es pot iniciar una venda si no hi ha una sessió iniciada"

  Escenari: No es pot crear un administrador si no hi ha una sessió iniciada
    Quan vull donar d'alta un nou administrador amb nom "Jefe2" i contrasenya "123"
    Aleshores obtinc un error que diu: "No es pot donar d'alta un nou administrador si no hi ha cap sessió iniciada"

  Escenari: No es pot crear un cap de botiga si no hi ha una sessió iniciada
    Quan vull donar d'alta un nou cap de botiga amb nom "Mano" i contrasenya "123"
    Aleshores obtinc un error que diu: "No es pot donar d'alta un nou cap de botiga si no hi ha cap sessió iniciada"

  Escenari: No es pot crear un treballador si no hi ha una sessió iniciada
    Quan vull donar d'alta un nou treballador amb nom "Marc" i contrasenya "123"
    Aleshores obtinc un error que diu: "No es pot donar d'alta un nou treballador si no hi ha cap sessió iniciada"

  Escenari: No es pot afegir un producte a una nova venta sense haver iniciat sessió
    Quan afegeixo el producte de codi de barres 321 a la venta
    Aleshores obtinc un error que diu: "No es pot iniciar una venda si no hi ha una sessió iniciada"

  Escenari: No es pot afegir un descompte percentual sense haver iniciat sessió
    Quan vull donar de alta un descompte del tipus "Percentual" de "30 %" per a tota la venta sense data indicada
    Aleshores obtinc un error que diu: "No es pot donar d'alta un descompte si no hi ha una sessio iniciada"

  Escenari: No es pot afegir un descompte fix sense haver iniciat sessió
    Quan vull donar de alta un descompte del tipus "Fix" de "1 €" per al producte "Playmobil" sense data indicada
    Aleshores obtinc un error que diu: "No es pot donar d'alta un descompte si no hi ha una sessio iniciada"

  Escenari: No es pot afegir un descompte NxM sense haver iniciat sessió
    Quan vull donar de alta un descompte del tipus "NxM" de "3x2" per al producte "Playmobil" sense data indicada
    Aleshores obtinc un error que diu: "No es pot donar d'alta un descompte si no hi ha una sessio iniciada"

  Escenari: No es pot afegir un descompte regal sense haver iniciat sessió
    Quan vull donar de alta un descompte del tipus "Regal" de "giftedProducts" per al producte "Playmobil" sense data indicada
    Aleshores obtinc un error que diu: "No es pot donar d'alta un descompte si no hi ha una sessio iniciada"

  Escenari: No es pot donar d'alta un val de descompte sense haver iniciat sessió
    Quan vull donar de alta un val de descompte de 1€ amb pagament minim de 3€ amb data inici "1/1/2015" i data fi "1/1/2017" i amb codi de barres 123
    Aleshores obtinc un error que diu: "No es pot donar d'alta un val de descompte si no hi ha una sessio iniciada"

  Escenari: No es pot imprimir cap ticket sense haver iniciat sessió
    Quan vull generar un tiquet
    Aleshores obtinc un error que diu: "No es pot imprimir un tiquet si no hi ha una sessió iniciada"
    
  Escenari: No es pot iniciar una jornada sense haver iniciat sessió
    Quan inicio un nou torn amb 1000€
    Aleshores obtinc un error que diu: "No es pot iniciar una jornada si no hi ha una sessió iniciada"