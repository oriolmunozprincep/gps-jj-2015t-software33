# language: ca

#noinspection SpellCheckingInspection
Característica: Tiquets amb descomptes

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Joan" amb contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Prime", preu 23€, iva 21% i codi de barres 1
    I un producte amb nom "Second", marca "DERP", preu 18€, iva 0% i codi de barres 2
    I un producte amb nom "Third", marca "LEG", preu 2€, iva 0% i codi de barres 3
    I un producte amb nom "Fourth", marca "PLAY", preu 20€, iva 10% i codi de barres 4
    I va fer logout
    I que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"
    I que s'ha iniciat un torn amb 500€
    I que hi ha una venta iniciada amb data "25/11/2015" i hora "10:00"


  Escenari: Tiquet amb un descompte percentual
    Donat que hi ha un descompte percentual del 10% sobre el producte "Fourth"
    I afegeixo el producte de codi de barres 4 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticketWithPercentualDiscount"

  Escenari: Tiquet amb un descompte 2x1 per marca
    Donat que hi ha un 2x1 en "LEG"
    I afegeixo el producte de codi de barres 3 a la venta
    I afegeixo el producte de codi de barres 3 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticketWith2x1DiscountByBrand"

  Escenari: Ticket amb un descompte 2x1 per producte
    Donat que hi ha un 2x1 en el producte "Third"
    I afegeixo el producte de codi de barres 3 a la venta
    I afegeixo el producte de codi de barres 3 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticketWith2x1DiscountByProduct"

  Escenari: Tiquet amb descompte fix
    Donat que hi ha un descompte fixe de 10€ sobre la venta
    I afegeixo el producte de codi de barres 1 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticketWithFixDiscount"

  Escenari: Tiquet amb regal
    Donat que per la compra d'un producte "Prime" es regala una unitat del producte "Third"
    I afegeixo el producte de codi de barres 3 a la venta
    I afegeixo el producte de codi de barres 1 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticketWithGift"

  Escenari: Tiquet amb potpourri de descomptes
    Donat que per la compra d'un producte "Prime" es regala una unitat del producte "Third"
    I afegeixo el producte de codi de barres 3 a la venta
    I afegeixo el producte de codi de barres 1 a la venta
    I que hi ha un descompte percentual del 10% sobre el producte "Fourth"
    I afegeixo el producte de codi de barres 4 a la venta
    I que hi ha un 2x1 en "DERP"
    I afegeixo el producte de codi de barres 2 a la venta
    I afegeixo el producte de codi de barres 2 a la venta
    Quan vull generar un tiquet
    Aleshores es genera un tiquet amb tota la informació de la venta igual a "ticketDiscountPotpourri"