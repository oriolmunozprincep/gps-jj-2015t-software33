# language: ca

#noinspection SpellCheckingInspection
Característica: Finalitzar jornada i indicar efectiu final

  Rerefons:
    Donat que estem al tpv número 1 de la botiga "Girona 1" amb adreça "Adreça 27"
    I que hi ha un usuari administrador "Jefe" amb contrasenya "123"
    I que hi ha un treballador amb nom d'usuari "Joan" i contrasenya "123"
    I que hi ha un cap de botiga "ProdManager" amb contrassenya "123"
    I que en "ProdManager" ha iniciat la sessio al tpv amb contrasenya "123"
    I un producte amb nom "Optimus Prime", preu 23€, iva 21% i codi de barres 1234567
    I un producte amb nom "Second", preu 19,9€, iva 15% i codi de barres 98765
    I un producte amb nom "Third", preu 0,5€, iva 15% i codi de barres 654321
    I va fer logout
    I que en "Joan" ha iniciat la sessio al tpv amb contrasenya "123"

  Escenari: Tancar un torn amb desquadrament 0
    Donat que s'ha iniciat un torn amb 500€
    I inicio una nova venta
    I afegeixo el producte de codi de barres 1234567 a la venta
    I el client vol pagar amb "Efectiu" i que el client aporta 27,83€ i vull calcular el canvi a tornar
    Quan tanco el torn indicant que hi han 527,83€ a caixa
    Aleshores no sortira cap missatge d'error
    I es guarda el missatge "Quadrament correcte"
    I amb descuadrament 0,0€

  Escenari: Tancar un torn amb desquadrament, recomptar i quadrar be
    Donat que s'ha iniciat un torn amb 500€
    I tanco el torn indicant que hi han 505,24€ a caixa
    I obtinc un error que diu: "Quadrament incorrecte"
    Quan faig el recompte indican't que hi han 500€ a caixa
    Aleshores no sortira cap missatge d'error
    I es guarda el missatge "Quadrament correcte"
    I amb descuadrament 0,0€

  Escenari: Tancar un torn amb desquadrament i superar el limit de reintents
    Donat que s'ha iniciat un torn amb 500€
    I tanco el torn indicant que hi han 505,24€ a caixa
    I obtinc un error que diu: "Quadrament incorrecte"
    I llavors fare recompte i veure que hi han 498,88€ a caixa
    I obtinc un error que diu: "Quadrament incorrecte"
    I llavors fare recompte i veure que hi han 498,98€ a caixa
    I obtinc un error que diu: "Quadrament incorrecte"
    Quan faig el recompte indican't que hi han 501,58€ a caixa
    Aleshores obtinc un error que diu: "No hi han mes reintents de quadrament"
    I es guarda el missatge "Maxim reintents"
    I amb descuadrament 1,58€

  Escenari: Tancar torn amb desquadrament i missatge
    Donat que s'ha iniciat un torn amb 500€
    I inicio una nova venta
    I afegeixo el producte de codi de barres 1234567 a la venta
    I el client vol pagar amb "Targeta" i vull calcular el canvi a tornar
    I tanco el torn indicant que hi han 505,24€ a caixa
    I obtinc un error que diu: "Quadrament incorrecte"
    Quan tanqui la jornada amb els 518,24€ i deixant el missatge "No se comptar"
    I es guarda el missatge "No se comptar"
    I amb descuadrament 9,59€