# GPS Software 33

Sistema TPV per a Joguines Juguets.

## Components del grup

- Cristina Fontanet Albinyana - cristina.fontanet@est.fib.upc.edu
- Alejandro Hidalgo Berlango - alejandro.hidalgo@est.fib.upc.edu
- Rafael Lucena Araujo - rafael.lucena@est.fib.upc.edu
- Aleix Pellisa Cortiella - aleix.pellisa@est.fib.upc.edu
- Oriol Muñoz Princep - oriol.munoz.princep@est.fib.upc.edu
- Sergi Tortosa Ortiz-Villajos- sergi.tortosa@est.fib.upc.edu

## Enllaços

- [Backlog](https://bitbucket.org/oriolmunozprincep/gps-jj-2015t-software33/wiki/Backlog)